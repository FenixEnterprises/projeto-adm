﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dona_Cherry.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Dona_Cherry.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _07f78d3820c22e0e01e4e7186806f5b2 {
            get {
                object obj = ResourceManager.GetObject("07f78d3820c22e0e01e4e7186806f5b2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _1_Bowl_Vegan_Gluten_Free_Vanilla_Cake_Egg_free_oil_free_fluffy_perfectly_sweet_SO_delicious_vegan_glutenfree_cake_recipe_plantbased_minimal {
            get {
                object obj = ResourceManager.GetObject("1-Bowl-Vegan-Gluten-Free-Vanilla-Cake-Egg-free-oil-free-fluffy-perfectly-sweet-SO" +
                        "-delicious-vegan-glutenfree-cake-recipe-plantbased-minimal", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _1_Bowl_Vegan_Gluten_Free_Vanilla_Cake_Egg_free_oil_free_fluffy_perfectly_sweet_SO_delicious_vegan_glutenfree_cake_recipe_plantbased_minimalistbaker_9 {
            get {
                object obj = ResourceManager.GetObject("1-Bowl-Vegan-Gluten-Free-Vanilla-Cake-Egg-free-oil-free-fluffy-perfectly-sweet-SO" +
                        "-delicious-vegan-glutenfree-cake-recipe-plantbased-minimalistbaker-9", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _14cd0a5041271c21227c8b24a6040c40 {
            get {
                object obj = ResourceManager.GetObject("14cd0a5041271c21227c8b24a6040c40", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _3067216a540894b93b0a4a07ddb36dd6__beautiful_cakes_amazing_cakes {
            get {
                object obj = ResourceManager.GetObject("3067216a540894b93b0a4a07ddb36dd6--beautiful-cakes-amazing-cakes", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _3551772ddf3dda996c208539e00bbac5 {
            get {
                object obj = ResourceManager.GetObject("3551772ddf3dda996c208539e00bbac5", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _3551772ddf3dda996c208539e00bbac51 {
            get {
                object obj = ResourceManager.GetObject("3551772ddf3dda996c208539e00bbac51", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _3551772ddf3dda996c208539e00bbac52 {
            get {
                object obj = ResourceManager.GetObject("3551772ddf3dda996c208539e00bbac52", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _3955A54C_E068_4319_A265_DCDC20ABF861 {
            get {
                object obj = ResourceManager.GetObject("3955A54C-E068-4319-A265-DCDC20ABF861", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _515838fd5a66095 {
            get {
                object obj = ResourceManager.GetObject("515838fd5a66095", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _515838fd5a660951 {
            get {
                object obj = ResourceManager.GetObject("515838fd5a660951", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _515838fd5a660952 {
            get {
                object obj = ResourceManager.GetObject("515838fd5a660952", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _51bf14c4cbdd1bf617ed8ed7742759f5_decora__o_divisor_de_linha_ornamentado_by_vexels {
            get {
                object obj = ResourceManager.GetObject("51bf14c4cbdd1bf617ed8ed7742759f5-decora--o-divisor-de-linha-ornamentado-by-vexels" +
                        "", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _5303_VeganChocolateCherryLayerCake_Cut_1120 {
            get {
                object obj = ResourceManager.GetObject("5303-VeganChocolateCherryLayerCake_Cut-1120", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _64456_Cherry_Pound_Cake {
            get {
                object obj = ResourceManager.GetObject("64456-Cherry-Pound-Cake", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _64456_Cherry_Pound_Cake1 {
            get {
                object obj = ResourceManager.GetObject("64456-Cherry-Pound-Cake1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _7357af1851ea8d9 {
            get {
                object obj = ResourceManager.GetObject("7357af1851ea8d9", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _92560f79677c44f {
            get {
                object obj = ResourceManager.GetObject("92560f79677c44f", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap aaaaaaaaaaaaaaaaa {
            get {
                object obj = ResourceManager.GetObject("aaaaaaaaaaaaaaaaa", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap aaaaaaaaaaaaaaaaa1 {
            get {
                object obj = ResourceManager.GetObject("aaaaaaaaaaaaaaaaa1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap aaaaaaaaaaaaaaaaa2 {
            get {
                object obj = ResourceManager.GetObject("aaaaaaaaaaaaaaaaa2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ab287fe360c113b515568f73f40ffaaa {
            get {
                object obj = ResourceManager.GetObject("ab287fe360c113b515568f73f40ffaaa", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ab287fe360c113b515568f73f40ffaaa1 {
            get {
                object obj = ResourceManager.GetObject("ab287fe360c113b515568f73f40ffaaa1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap aba {
            get {
                object obj = ResourceManager.GetObject("aba", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Adicionar_ao_carrinho {
            get {
                object obj = ResourceManager.GetObject("Adicionar ao carrinho", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ALTERAR {
            get {
                object obj = ResourceManager.GetObject("ALTERAR", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap AMAZING_1_Bowl_Chocolate_Cake_with_Coconut_Whipped_Cream_with_Berries_The_perfect_summer_birthday_cake_vegan_glutenfree_cake_glutenfree_chocolate_birthday {
            get {
                object obj = ResourceManager.GetObject("AMAZING-1-Bowl-Chocolate-Cake-with-Coconut-Whipped-Cream-with-Berries-The-perfect" +
                        "-summer-birthday-cake-vegan-glutenfree-cake-glutenfree-chocolate-birthday", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap babypink {
            get {
                object obj = ResourceManager.GetObject("babypink", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap background_para_blog_marrom_com_rosa_cute_kawaii_photoscape_shopping_by_thataschultz20110924_fundo_me_r_11 {
            get {
                object obj = ResourceManager.GetObject("background-para-blog-marrom-com-rosa-cute-kawaii-photoscape-shopping-by-thataschu" +
                        "ltz20110924-fundo-me-r-11", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap barraloader {
            get {
                object obj = ResourceManager.GetObject("barraloader", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Black_Forest_Cake_17_Edit {
            get {
                object obj = ResourceManager.GetObject("Black-Forest-Cake-17-Edit", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Black_Forest_Cake_5 {
            get {
                object obj = ResourceManager.GetObject("Black-Forest-Cake-5", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Black_Forest_Cake_51 {
            get {
                object obj = ResourceManager.GetObject("Black-Forest-Cake-51", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Botao_Salvar {
            get {
                object obj = ResourceManager.GetObject("Botao Salvar", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap c51f51b34442c2ee35bc462dcdf0e257 {
            get {
                object obj = ResourceManager.GetObject("c51f51b34442c2ee35bc462dcdf0e257", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap CapturarSplash {
            get {
                object obj = ResourceManager.GetObject("CapturarSplash", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ceia_vegana_de_natal_e_ano_novo_6 {
            get {
                object obj = ResourceManager.GetObject("ceia-vegana-de-natal-e-ano-novo_6", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ceia_vegana_de_natal_e_ano_novo_61 {
            get {
                object obj = ResourceManager.GetObject("ceia-vegana-de-natal-e-ano-novo_61", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Cookie_Dough_Cake_SQ1 {
            get {
                object obj = ResourceManager.GetObject("Cookie-Dough-Cake-SQ1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ENTRAR {
            get {
                object obj = ResourceManager.GetObject("ENTRAR", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap escalope4 {
            get {
                object obj = ResourceManager.GetObject("escalope4", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap flourish_clipart_7 {
            get {
                object obj = ResourceManager.GetObject("flourish-clipart-7", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Flourish_Monogram_10 {
            get {
                object obj = ResourceManager.GetObject("Flourish Monogram 10", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap fundo_transparente {
            get {
                object obj = ResourceManager.GetObject("fundo_transparente", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap generatedtext {
            get {
                object obj = ResourceManager.GetObject("generatedtext", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap generatedtext__1_ {
            get {
                object obj = ResourceManager.GetObject("generatedtext (1)", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap IMG_20180827_WA0000 {
            get {
                object obj = ResourceManager.GetObject("IMG-20180827-WA0000", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap IMG_20180827_WA0001 {
            get {
                object obj = ResourceManager.GetObject("IMG-20180827-WA0001", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap IMG_20180827_WA0002 {
            get {
                object obj = ResourceManager.GetObject("IMG-20180827-WA0002", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap IMG_20180827_WA0003 {
            get {
                object obj = ResourceManager.GetObject("IMG-20180827-WA0003", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap IMG_20180827_WA0004 {
            get {
                object obj = ResourceManager.GetObject("IMG-20180827-WA0004", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap IMG_20180827_WA0005 {
            get {
                object obj = ResourceManager.GetObject("IMG-20180827-WA0005", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap IMG_20180827_WA0006 {
            get {
                object obj = ResourceManager.GetObject("IMG-20180827-WA0006", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap large {
            get {
                object obj = ResourceManager.GetObject("large", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap original {
            get {
                object obj = ResourceManager.GetObject("original", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap pancarli_kek {
            get {
                object obj = ResourceManager.GetObject("pancarli-kek", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Realizar_Pedido {
            get {
                object obj = ResourceManager.GetObject("Realizar Pedido", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap receitas_de_bolo_de_cenoura_2 {
            get {
                object obj = ResourceManager.GetObject("receitas-de-bolo-de-cenoura-2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap receitas_de_bolo_de_cenoura_21 {
            get {
                object obj = ResourceManager.GetObject("receitas-de-bolo-de-cenoura-21", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Red_Velvet_Cake_Batter_Cupcakes {
            get {
                object obj = ResourceManager.GetObject("Red-Velvet-Cake-Batter-Cupcakes", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Red_Velvet_Cake_Batter_Cupcakes1 {
            get {
                object obj = ResourceManager.GetObject("Red-Velvet-Cake-Batter-Cupcakes1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap REMOVER {
            get {
                object obj = ResourceManager.GetObject("REMOVER", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Schoko_KIrschkuchen5 {
            get {
                object obj = ResourceManager.GetObject("Schoko-KIrschkuchen5", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Seja_Bem_Vinda {
            get {
                object obj = ResourceManager.GetObject("Seja Bem Vinda", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Seja_Bem_Vinda1 {
            get {
                object obj = ResourceManager.GetObject("Seja Bem Vinda1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap striped_background1 {
            get {
                object obj = ResourceManager.GetObject("striped background1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap striped_background11 {
            get {
                object obj = ResourceManager.GetObject("striped background11", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap striped_background12 {
            get {
                object obj = ResourceManager.GetObject("striped background12", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap tumblr_omynhnrqZI1vdnvs9o1_500 {
            get {
                object obj = ResourceManager.GetObject("tumblr_omynhnrqZI1vdnvs9o1_500", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap tumblr_p2944klTW41snfnawo1_500 {
            get {
                object obj = ResourceManager.GetObject("tumblr_p2944klTW41snfnawo1_500", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap vegan_red_velvet_cake_9 {
            get {
                object obj = ResourceManager.GetObject("vegan-red-velvet-cake-9", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap VOLTAR {
            get {
                object obj = ResourceManager.GetObject("VOLTAR", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap wedding_cakes_around_the_world_with_delicious_desserts_tripelonia_unbelievable_top_most_popular_to_bake {
            get {
                object obj = ResourceManager.GetObject("wedding-cakes-around-the-world-with-delicious-desserts-tripelonia-unbelievable-to" +
                        "p-most-popular-to-bake", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap white_flourish_frame_hi {
            get {
                object obj = ResourceManager.GetObject("white-flourish-frame-hi", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
