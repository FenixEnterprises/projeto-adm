﻿using Dona_Cherry.Telas;
using Dona_Cherry.Telas.Cliente;
using Dona_Cherry.Telas.Controle_Financeiro;
using Dona_Cherry.Telas.Estoque;
using Dona_Cherry.Telas.Fornecedor;
using Dona_Cherry.Telas.Funcionário;
using Dona_Cherry.Telas.PedidoFornecedor;
using Dona_Cherry.Telas.Produto;
using Dona_Cherry.Telas.ProdutoVenda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry
{
    public partial class FrmDonaCherry : Form
    {
        public FrmDonaCherry()
        {
            InitializeComponent();
        }

        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.permissaorh == true)
            {


                if (UserSession.UsuarioLogado.permissaofinanceiro == true)
                {

                }

                if (UserSession.UsuarioLogado.permissaofuncionario == true)
                {

                }
            }
        }

        

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void btnFluxodeCaixa_Click(object sender, EventArgs e)
        {
            Fluxo_de_Caixa fluxo = new Fluxo_de_Caixa();
            fluxo.Show();
            this.Hide();
        }

        private void btnPedidoForn_Click(object sender, EventArgs e)
        {
            ListaPedido pedidoFornecedor = new ListaPedido();
            pedidoFornecedor.Show();
            this.Hide();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            MenuBolos menu = new MenuBolos();
            menu.Show();
            this.Hide();
        }

        private void btnCadFornecedor_Click(object sender, EventArgs e)
        {
            Cadastro_Fornecedor cadastro_Fornecedor = new Cadastro_Fornecedor();
            cadastro_Fornecedor.Show();
            this.Hide();
        }

        private void btnFolhaPgto_Click(object sender, EventArgs e)
        {
            FolhaDePagamento folhaDePagamento = new FolhaDePagamento();
            folhaDePagamento.Show();
            this.Hide();
        }

        private void btnRealizarPedido_Click(object sender, EventArgs e)
        {
            RealizarPedido realizarPedido = new RealizarPedido();
            realizarPedido.Show();
            this.Hide();
        }

        private void btnCadastrarcliente_Click(object sender, EventArgs e)
        {
            Cadastrar_Cliente cadastrar_Cliente = new Cadastrar_Cliente();
            cadastrar_Cliente.Show();
            this.Hide();
        }

        private void btnConsultarPedCliente_Click(object sender, EventArgs e)
        {
            Pedido_Consultar pedido_Consultar = new Pedido_Consultar();
            pedido_Consultar.Show();
            this.Hide();
        }

        private void btnConsultarFornecedor_Click(object sender, EventArgs e)
        {
            ConsultarFornecedor consultarFornecedor = new ConsultarFornecedor();
            consultarFornecedor.Show();
            this.Hide();
        }

        private void btnConsPedidoFornecedor_Click(object sender, EventArgs e)
        {
            ConsultarPedidoFornecedor consultarPedidoFornecedor = new ConsultarPedidoFornecedor();
            consultarPedidoFornecedor.Show();
            this.Hide();
        }

        private void btnCadProduto_Click(object sender, EventArgs e)
        {
            Cadastrar_Produto cadastrar_Produto = new Cadastrar_Produto();
            cadastrar_Produto.Show();
            this.Hide();
        }

        private void FrmDonaCherry_Load(object sender, EventArgs e)
        {

        }

        private void btnCadastrarFuncionario_Click(object sender, EventArgs e)
        {
            Cadastrar_Funcionario funcionario = new Cadastrar_Funcionario();
            funcionario.Show();
            this.Hide();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastrar_Funcionario funcionario = new Cadastrar_Funcionario();
            funcionario.Show();
            this.Hide();

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Consultar_Funcionário nn = new Consultar_Funcionário();
            nn.Show();
            this.Hide();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login_Funcionario mm = new Login_Funcionario();
            mm.Show();
            this.Hide();
        }

        private void alterarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alterar_Funcionario jj = new Alterar_Funcionario();
            jj.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Cadastro_Fornecedor oo = new Cadastro_Fornecedor();
            oo.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarFornecedor ll = new ConsultarFornecedor();
            ll.Show();
            this.Hide();
        }

        private void alterarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Alterar_Fornecedor çç = new Alterar_Fornecedor();
            çç.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Cadastrar_Cliente aa = new Cadastrar_Cliente();
            aa.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Consultar_Cliente ww = new Consultar_Cliente();
            ww.Show();
            this.Hide();
        }

        private void alterarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Cliente_Alterar mm = new Cliente_Alterar();
            mm.Show();
            this.Hide();
        }

        private void realizarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RealizarPedido pp = new RealizarPedido();
            pp.Show();
            this.Hide();
        }

        private void consultarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pedido_Consultar vv = new Pedido_Consultar();
            vv.Show();
            this.Hide();
        }

        private void listaDePedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaPedido qq = new ListaPedido();
            qq.Show();
            this.Hide();
        }

        private void consultarListaDePedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarPedidoFornecedor hh = new ConsultarPedidoFornecedor();
            hh.Show();
            this.Hide();
        }

        private void registarComprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistrarCompras ss = new RegistrarCompras();
            ss.Show();
            this.Hide();
        }

        private void consultarComprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarCompras dd = new ConsultarCompras();
            dd.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            CadastroEstoque yy = new CadastroEstoque();
            yy.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ConsultaEstoque zz = new ConsultaEstoque();
            zz.Show();
            this.Hide();
        }

        private void atualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroEstoque xx = new CadastroEstoque();
            xx.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Fluxo_de_Caixa uu = new Fluxo_de_Caixa();
            uu.Show();
            this.Hide();
        }

        private void calcularToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolhaDePagamento tt = new FolhaDePagamento();
            tt.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            FolhaConsultar ff = new FolhaConsultar();
            ff.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Cadastrar_Produto kk = new Cadastrar_Produto();
            kk.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            ConsultaEstoque mm = new ConsultaEstoque();
            mm.Show();
            this.Hide();
        }

        private void alterarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            AlterarProduto bb = new AlterarProduto();
            bb.Show();
            this.Hide();
        }

        private void cadastrarCadárpioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarCardápio rr = new CadastrarCardápio();
            rr.Show();
            this.Hide();
        }

        private void menuBolosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MenuBolos cc = new MenuBolos();
            cc.Show();
            this.Hide();
        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
