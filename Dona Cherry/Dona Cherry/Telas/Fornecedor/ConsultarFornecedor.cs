﻿using Dona_Cherry.Telas.PedidoCliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.Fornecedor
{
    public partial class ConsultarFornecedor : Form
    {
        public ConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)

        {
            try
            {
                FornecedorBusiness business = new FornecedorBusiness();
                List<FornecedorDTO> lista = business.Consultar(txtfornecedor.Text);

                dgvfornecedor.AutoGenerateColumns = false;
                dgvfornecedor.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry voltar = new FrmDonaCherry();
            voltar.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvfornecedor.CurrentRow != null)
                {
                    FornecedorDTO fornecedor = dgvfornecedor.CurrentRow.DataBoundItem as FornecedorDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse fornecedor?", "Dona Cherry",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        FornecedorBusiness business = new FornecedorBusiness();
                        business.Remover(fornecedor.id_fornecedor);

                        List<FornecedorDTO> a = business.Consultar(txtfornecedor.Text);
                        dgvfornecedor.AutoGenerateColumns = false;
                        dgvfornecedor.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um Fornecedor");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvfornecedor.CurrentRow != null)
                {
                    FornecedorDTO fornecedor = dgvfornecedor.CurrentRow.DataBoundItem as FornecedorDTO;

                    Alterar_Fornecedor tela = new Alterar_Fornecedor();
                    tela.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Selecione um Fornecedor, por favor.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorBusiness bus = new FornecedorBusiness();
                List<FornecedorDTO> lista = bus.Listar();

                dgvfornecedor.AutoGenerateColumns = false;
                dgvfornecedor.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void txtfornecedor_TextChanged(object sender, EventArgs e)
        {

        }
    }

}
            
    

    
