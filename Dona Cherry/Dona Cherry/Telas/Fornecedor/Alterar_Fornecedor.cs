﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.Fornecedor
{
    public partial class Alterar_Fornecedor : Form
    {
        public Alterar_Fornecedor()
        {
            InitializeComponent();
            
        }
       
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto = new FornecedorDTO();

                dto.nm_fantasia = txtnomefantasia.Text;
                dto.nm_razaosocial = txtrazaosocial.Text;
                dto.ds_cidade = txtcidade.Text;
                dto.ds_CEP = txtcep.Text;
                dto.ds_bairro = txtbairro.Text;
                dto.ds_estado = lstEstado.Text;
                dto.ds_complemento = txtcomplemento.Text;
                dto.ds_CNPJ = txtcnpj.Text;
                dto.ds_endereco = txtendereco.Text;

                FornecedorBusiness business = new FornecedorBusiness();
                business.Alterar(dto);


                MessageBox.Show("Fornecedor alterado com sucesso");

                ConsultarFornecedor tela = new ConsultarFornecedor();
                tela.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void Alterar_Fornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmDonaCherry vc = new FrmDonaCherry();
            vc.Show();
            this.Hide();
        }

       FornecedorDTO Fornece;

        //public void LoadScreen(FornecedorDTO  Forne)
        //{
        //    this.Fornece = Forne;

        //    txtnomefantasia.Text = this.Fornece.nm_fantasia;
        //    txtbairro.Text = this.Fornece.ds_bairro;
        //    txtcnpj.Text = this.Fornece.ds_CNPJ;
        //    txtcep.Text = this.Fornece.ds_CEP;
        //    txtcidade.Text = this.Fornece.ds_cidade;
        //    txtcomplemento.Text = this.Fornece.ds_complemento;
        //    txtendereco.Text = this.Fornece.ds_endereco;
        //    lstEstado.Text = this.Fornece.ds_estado;
        //    txtrazaosocial.Text = this.Fornece.nm_razaosocial;

        //}

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
                

        //        Fornece.nm_fantasia = txtnomefantasia.Text;
        //        Fornece.nm_razaosocial = txtrazaosocial.Text;
        //        Fornece.ds_cidade = txtcidade.Text;
        //        Fornece.ds_CEP = txtcep.Text;
        //        Fornece.ds_bairro = txtbairro.Text;
        //        Fornece.ds_estado = lstEstado.Text;
        //        Fornece.ds_complemento = txtcomplemento.Text;
        //        Fornece.ds_CNPJ = txtcnpj.Text;
        //        Fornece.ds_endereco = txtendereco.Text;

        //        FornecedorBusiness business = new FornecedorBusiness();
        //        business.Alterar(Fornece);


        //        MessageBox.Show("Fornecedor alterado com sucesso");

        //        ConsultarFornecedor tela = new ConsultarFornecedor();
        //        tela.Show();
        //        this.Hide();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Ocorreu um erro: " + ex.Message);
        //    }
        }
    }

