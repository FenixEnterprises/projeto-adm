﻿using Dona_Cherry.Telas.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry
{
    public partial class Cadastro_Fornecedor : Form
    {
        public Cadastro_Fornecedor()
        {
            InitializeComponent();
        }

        private void Cadastro_Fornecedor_Load(object sender, EventArgs e)
        {
        }

        private void lblNomeFantasia_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblComplemento_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblCPF_Click(object sender, EventArgs e)
        {

        }

        private void lblCNPJ_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
           

        
    }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void maskedTextBox5_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry tela = new FrmDonaCherry();
            tela.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO cadastro = new FornecedorDTO();

                FornecedorDTO dto = new FornecedorDTO();
                dto.nm_fantasia = txtNmFantasia.Text;
                dto.nm_razaosocial = txtRazaoSocial.Text;
                dto.ds_cidade = txtCidade.Text;
                dto.ds_CEP = txtCEP.Text;
                dto.ds_bairro = txtBairro.Text;
                dto.ds_estado = lstEstado.Text;
                dto.ds_complemento = txtComplemento.Text;
                dto.ds_CNPJ = txtCPFCNPJ.Text;
                dto.ds_endereco = txtendereco.Text;



                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);

                MessageBox.Show("Salvo com sucesso.", "Dona Cherry",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                FrmDonaCherry nv = new FrmDonaCherry();
                nv.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
          
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
    }

