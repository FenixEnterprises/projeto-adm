﻿namespace Dona_Cherry.Telas.Fornecedor
{
    partial class Alterar_Fornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnomefantasia = new System.Windows.Forms.Label();
            this.lblrazaosocial = new System.Windows.Forms.Label();
            this.lblcnpj = new System.Windows.Forms.Label();
            this.lblendereco = new System.Windows.Forms.Label();
            this.lblcep = new System.Windows.Forms.Label();
            this.lblestado = new System.Windows.Forms.Label();
            this.lblcidade = new System.Windows.Forms.Label();
            this.lblbairro = new System.Windows.Forms.Label();
            this.txtnomefantasia = new System.Windows.Forms.TextBox();
            this.txtrazaosocial = new System.Windows.Forms.TextBox();
            this.txtendereco = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtcnpj = new System.Windows.Forms.MaskedTextBox();
            this.txtcep = new System.Windows.Forms.MaskedTextBox();
            this.lstEstado = new System.Windows.Forms.ListBox();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.lblcomplemento = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblnomefantasia
            // 
            this.lblnomefantasia.AutoSize = true;
            this.lblnomefantasia.BackColor = System.Drawing.Color.Transparent;
            this.lblnomefantasia.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnomefantasia.ForeColor = System.Drawing.Color.Black;
            this.lblnomefantasia.Location = new System.Drawing.Point(6, 18);
            this.lblnomefantasia.Name = "lblnomefantasia";
            this.lblnomefantasia.Size = new System.Drawing.Size(190, 27);
            this.lblnomefantasia.TabIndex = 15;
            this.lblnomefantasia.Text = "Nome Fantasia";
            // 
            // lblrazaosocial
            // 
            this.lblrazaosocial.AutoSize = true;
            this.lblrazaosocial.BackColor = System.Drawing.Color.Transparent;
            this.lblrazaosocial.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrazaosocial.Location = new System.Drawing.Point(25, 55);
            this.lblrazaosocial.Name = "lblrazaosocial";
            this.lblrazaosocial.Size = new System.Drawing.Size(162, 27);
            this.lblrazaosocial.TabIndex = 14;
            this.lblrazaosocial.Text = "Razão Social";
            // 
            // lblcnpj
            // 
            this.lblcnpj.AutoSize = true;
            this.lblcnpj.BackColor = System.Drawing.Color.Transparent;
            this.lblcnpj.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcnpj.Location = new System.Drawing.Point(110, 88);
            this.lblcnpj.Name = "lblcnpj";
            this.lblcnpj.Size = new System.Drawing.Size(77, 27);
            this.lblcnpj.TabIndex = 13;
            this.lblcnpj.Text = "CNPJ";
            // 
            // lblendereco
            // 
            this.lblendereco.AutoSize = true;
            this.lblendereco.BackColor = System.Drawing.Color.Transparent;
            this.lblendereco.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblendereco.Location = new System.Drawing.Point(73, 115);
            this.lblendereco.Name = "lblendereco";
            this.lblendereco.Size = new System.Drawing.Size(118, 27);
            this.lblendereco.TabIndex = 12;
            this.lblendereco.Text = "Endereço";
            // 
            // lblcep
            // 
            this.lblcep.AutoSize = true;
            this.lblcep.BackColor = System.Drawing.Color.Transparent;
            this.lblcep.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcep.Location = new System.Drawing.Point(128, 150);
            this.lblcep.Name = "lblcep";
            this.lblcep.Size = new System.Drawing.Size(59, 27);
            this.lblcep.TabIndex = 11;
            this.lblcep.Text = "CEP";
            // 
            // lblestado
            // 
            this.lblestado.AutoSize = true;
            this.lblestado.BackColor = System.Drawing.Color.Transparent;
            this.lblestado.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblestado.Location = new System.Drawing.Point(100, 191);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(89, 27);
            this.lblestado.TabIndex = 10;
            this.lblestado.Text = "Estado";
            // 
            // lblcidade
            // 
            this.lblcidade.AutoSize = true;
            this.lblcidade.BackColor = System.Drawing.Color.Transparent;
            this.lblcidade.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcidade.Location = new System.Drawing.Point(98, 232);
            this.lblcidade.Name = "lblcidade";
            this.lblcidade.Size = new System.Drawing.Size(90, 27);
            this.lblcidade.TabIndex = 9;
            this.lblcidade.Text = "Cidade";
            // 
            // lblbairro
            // 
            this.lblbairro.AutoSize = true;
            this.lblbairro.BackColor = System.Drawing.Color.Transparent;
            this.lblbairro.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbairro.Location = new System.Drawing.Point(103, 268);
            this.lblbairro.Name = "lblbairro";
            this.lblbairro.Size = new System.Drawing.Size(89, 27);
            this.lblbairro.TabIndex = 8;
            this.lblbairro.Text = "Bairro";
            // 
            // txtnomefantasia
            // 
            this.txtnomefantasia.Location = new System.Drawing.Point(189, 19);
            this.txtnomefantasia.Name = "txtnomefantasia";
            this.txtnomefantasia.Size = new System.Drawing.Size(225, 20);
            this.txtnomefantasia.TabIndex = 0;
            // 
            // txtrazaosocial
            // 
            this.txtrazaosocial.Location = new System.Drawing.Point(189, 55);
            this.txtrazaosocial.Name = "txtrazaosocial";
            this.txtrazaosocial.Size = new System.Drawing.Size(225, 20);
            this.txtrazaosocial.TabIndex = 1;
            // 
            // txtendereco
            // 
            this.txtendereco.Location = new System.Drawing.Point(189, 121);
            this.txtendereco.Name = "txtendereco";
            this.txtendereco.Size = new System.Drawing.Size(225, 20);
            this.txtendereco.TabIndex = 3;
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(189, 238);
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(225, 20);
            this.txtcidade.TabIndex = 6;
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(189, 274);
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(225, 20);
            this.txtbairro.TabIndex = 7;
            // 
            // txtcnpj
            // 
            this.txtcnpj.Location = new System.Drawing.Point(189, 88);
            this.txtcnpj.Mask = "99.999.999/9999-99";
            this.txtcnpj.Name = "txtcnpj";
            this.txtcnpj.Size = new System.Drawing.Size(105, 20);
            this.txtcnpj.TabIndex = 16;
            // 
            // txtcep
            // 
            this.txtcep.Location = new System.Drawing.Point(189, 156);
            this.txtcep.Mask = "00000-000";
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(60, 20);
            this.txtcep.TabIndex = 17;
            // 
            // lstEstado
            // 
            this.lstEstado.FormattingEnabled = true;
            this.lstEstado.Items.AddRange(new object[] {
            "SP",
            "RJ",
            "MG",
            "ES",
            "BA"});
            this.lstEstado.Location = new System.Drawing.Point(189, 191);
            this.lstEstado.Name = "lstEstado";
            this.lstEstado.Size = new System.Drawing.Size(57, 30);
            this.lstEstado.TabIndex = 19;
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(189, 310);
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(225, 20);
            this.txtcomplemento.TabIndex = 20;
            // 
            // lblcomplemento
            // 
            this.lblcomplemento.AutoSize = true;
            this.lblcomplemento.BackColor = System.Drawing.Color.Transparent;
            this.lblcomplemento.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcomplemento.Location = new System.Drawing.Point(32, 304);
            this.lblcomplemento.Name = "lblcomplemento";
            this.lblcomplemento.Size = new System.Drawing.Size(166, 27);
            this.lblcomplemento.TabIndex = 21;
            this.lblcomplemento.Text = "Complemento";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::Dona_Cherry.Properties.Resources.VOLTAR;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Transparent;
            this.button1.Location = new System.Drawing.Point(389, 368);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 37);
            this.button1.TabIndex = 23;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.lblnomefantasia);
            this.groupBox1.Controls.Add(this.txtnomefantasia);
            this.groupBox1.Controls.Add(this.txtrazaosocial);
            this.groupBox1.Controls.Add(this.lblcomplemento);
            this.groupBox1.Controls.Add(this.lblrazaosocial);
            this.groupBox1.Controls.Add(this.txtcomplemento);
            this.groupBox1.Controls.Add(this.txtcnpj);
            this.groupBox1.Controls.Add(this.lblbairro);
            this.groupBox1.Controls.Add(this.txtbairro);
            this.groupBox1.Controls.Add(this.lstEstado);
            this.groupBox1.Controls.Add(this.txtcidade);
            this.groupBox1.Controls.Add(this.lblcidade);
            this.groupBox1.Controls.Add(this.lblcnpj);
            this.groupBox1.Controls.Add(this.txtcep);
            this.groupBox1.Controls.Add(this.lblendereco);
            this.groupBox1.Controls.Add(this.lblestado);
            this.groupBox1.Controls.Add(this.txtendereco);
            this.groupBox1.Controls.Add(this.lblcep);
            this.groupBox1.Location = new System.Drawing.Point(79, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(433, 407);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(132, 357);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(143, 29);
            this.button2.TabIndex = 23;
            this.button2.Text = "Alterar";
            this.button2.UseVisualStyleBackColor = true;
            
            // 
            // Alterar_Fornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Dona_Cherry.Properties.Resources.babypink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(524, 434);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "Alterar_Fornecedor";
            this.Text = "frmAlterarFornecedor";
            this.Load += new System.EventHandler(this.Alterar_Fornecedor_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblnomefantasia;
        private System.Windows.Forms.Label lblrazaosocial;
        private System.Windows.Forms.Label lblcnpj;
        private System.Windows.Forms.Label lblendereco;
        private System.Windows.Forms.Label lblcep;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.Label lblcidade;
        private System.Windows.Forms.Label lblbairro;
        private System.Windows.Forms.TextBox txtnomefantasia;
        private System.Windows.Forms.TextBox txtrazaosocial;
        private System.Windows.Forms.TextBox txtendereco;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.MaskedTextBox txtcnpj;
        private System.Windows.Forms.MaskedTextBox txtcep;
        private System.Windows.Forms.ListBox lstEstado;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.Label lblcomplemento;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
    }
}