﻿using Dona_Cherry.Telas;
using Dona_Cherry.Telas.Funcionário;
using Dona_Cherry.Telas.PedidoFornecedor;
using Dona_Cherry.Telas.ProdutoVenda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();

            
            Task.Factory.StartNew(() =>
            {
                
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {

                    FrmDonaCherry login = new FrmDonaCherry();
                    login.Show();
                    Hide();
                }));
            });

        }

    

    private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Splash_Load(object sender, EventArgs e)
        {

        }
    }
}
