﻿using Dona_Cherry.DB.Estoque;
using Dona_Cherry.DB.ProdutoVenda;
using Dona_Cherry.DonaCherry;
using Dona_Cherry.Telas.Cliente;
using Dona_Cherry.Telas.PedidoCliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry
{
    public partial class RealizarPedido : Form
    {
        BindingList<ProdutoVendaDTO> produtosCarrinho = new BindingList<ProdutoVendaDTO>();
        BindingList<ClienteDTO> clientes = new BindingList<ClienteDTO>();

        public RealizarPedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }

        void CarregarCombos()
        {
            ProdutoVendaBusiness pdBusiness = new ProdutoVendaBusiness();
            List<ProdutoVendaDTO> ProdutoVenda = pdBusiness.Listar();

            cboproduto.ValueMember = nameof(ProdutoVendaDTO.id);
            cboproduto.DisplayMember = nameof(ProdutoVendaDTO.nome);
            cboproduto.DataSource = ProdutoVenda;


            ClienteBusiness clienteBusiness = new ClienteBusiness();
            List<ClienteDTO> clienteLista = clienteBusiness.Listar();
            clientes = new BindingList<ClienteDTO>(clienteLista);

            cbocliente.ValueMember = nameof(ClienteDTO.id_cliente);
            cbocliente.DisplayMember = nameof(ClienteDTO.nome);
            cbocliente.DataSource = clientes;
        }
        void ConfigurarGrid()
        {
            dgvcarrinho.AutoGenerateColumns = false;
            dgvcarrinho.DataSource = produtosCarrinho;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoVendaDTO dto = cboproduto.SelectedItem as ProdutoVendaDTO;

                int qtd = Convert.ToInt32(numericUpDown1.Value);

                for (int i = 0; i < qtd; i++)
                {
                    produtosCarrinho.Add(dto);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void RealizarPedido_Load(object sender, EventArgs e)
        {

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry voltar = new FrmDonaCherry();
            voltar.Show();
            this.Hide();
        }

        private void lstProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtQuantidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void lstCliente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lstFormadePgmt_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void lblCliente_Click(object sender, EventArgs e)
        {

        }

        private void lbltotal_Click(object sender, EventArgs e)
        {

        }

        private void lblcompra_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            //int quantia = Convert.ToInt32(numericUpDown1.Value);
            //decimal preco = 

            //PedidoClienteDatabase cal = new PedidoClienteDatabase();
            //decimal Total = cal.CalcularCompra(quantia, preco);

            //lblcompra.Text = "R$ " + Decimal.Round(Total).ToString();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {

            //Cadastrar_Cliente tela = new Cadastrar_Cliente();
            //tela.ShowDialog();

            //if (tela.ClienteCriado != null)
            //{
            //    clientes.Add(tela.ClienteCriado);
            //    cbocliente.SelectedItem = tela.ClienteCriado;
            //}
        }
        private void lblFormPagmento_Click(object sender, EventArgs e)
        {

        }

        private void txtcliente_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtproduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnemitirpedido_Click(object sender, EventArgs e)
        {
            try
            {

                ClienteDTO cliente = cbocliente.SelectedItem as ClienteDTO;
                ProdutoVendaDTO produtovenda = cboproduto.SelectedItem as ProdutoVendaDTO;

                PedidoClienteDTO dto = new PedidoClienteDTO();
                dto.funcionarioid = UserSession.UsuarioLogado.id_funcionario;
                dto.clienteid = cliente.id_cliente;
                dto.preco = Convert.ToDecimal(lblcompra.Text);
                dto.formadepagamento = lstFormadePgmt.Text;
                dto.data = DateTime.Now;

                PedidoClienteBusiness business = new PedidoClienteBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());


                EstoqueBusiness businessestoque = new EstoqueBusiness();
                List<EstoqueDTO> estoque = businessestoque.Listar();

                foreach (ProdutoVendaDTO item in produtosCarrinho)
                {
                    foreach (EstoqueDTO item2 in estoque)
                    {
                        if (item.id == item2.id_produto)
                        {
                            item2.qtd_totalprodutos = item2.qtd_totalprodutos - 1;
                        }
                    }
                }


                foreach (EstoqueDTO item in estoque)
                {
                    EstoqueDTO estoquedto = new EstoqueDTO();

                    estoquedto.id_produto = item.id_produto;
                    estoquedto.qtd_totalprodutos = item.qtd_totalprodutos;

                    businessestoque.Alterar(estoquedto);
                }




                MessageBox.Show("Pedido realizado com sucesso.", "Dona Cherry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();

            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboproduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProdutoVendaDTO dto = cboproduto.SelectedItem as ProdutoVendaDTO;
                imgProduto.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
            }
            catch
            {
            }
        }
    }
}
