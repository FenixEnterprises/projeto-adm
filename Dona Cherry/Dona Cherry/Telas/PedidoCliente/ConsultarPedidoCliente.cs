﻿using Dona_Cherry.Telas.Cliente;
using Dona_Cherry.Telas.PedidoCliente;
using Loja_de_roupas.DB.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry
{
    public partial class Pedido_Consultar : Form
    {
        public Pedido_Consultar()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry voltar = new FrmDonaCherry();
            voltar.Show();
            this.Hide();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ClienteDTO cliente = new ClienteDTO();
            cliente.nome = txtNomeCliente.Text;
           
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoClienteBusiness business = new PedidoClienteBusiness();
                List<PedidoConsultarView> lista = business.Consultar(txtNomeCliente.Text.Trim());

                dgvPedidoConsultar.AutoGenerateColumns = false;
                dgvPedidoConsultar.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
           
        }

        private void dgvPedidoConsultar_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
