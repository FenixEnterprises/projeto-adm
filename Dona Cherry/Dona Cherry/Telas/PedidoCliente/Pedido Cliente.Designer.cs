﻿namespace Dona_Cherry
{
    partial class RealizarPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblProduto = new System.Windows.Forms.Label();
            this.lblFormPagmento = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblQuantidade = new System.Windows.Forms.Label();
            this.lstFormadePgmt = new System.Windows.Forms.ListBox();
            this.btnrealizarpedido = new System.Windows.Forms.PictureBox();
            this.btnVoltar = new System.Windows.Forms.PictureBox();
            this.lblcompra = new System.Windows.Forms.Label();
            this.btnTotal = new System.Windows.Forms.Button();
            this.dgvcarrinho = new System.Windows.Forms.DataGridView();
            this.Produtos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboproduto = new System.Windows.Forms.ComboBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.btnemitirpedido = new System.Windows.Forms.Button();
            this.imgProduto = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbocliente = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnrealizarpedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcarrinho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblProduto
            // 
            this.lblProduto.AutoSize = true;
            this.lblProduto.BackColor = System.Drawing.Color.Transparent;
            this.lblProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblProduto.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduto.ForeColor = System.Drawing.Color.Black;
            this.lblProduto.Location = new System.Drawing.Point(193, 21);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.Size = new System.Drawing.Size(110, 27);
            this.lblProduto.TabIndex = 1;
            this.lblProduto.Text = "Produto";
            // 
            // lblFormPagmento
            // 
            this.lblFormPagmento.AutoSize = true;
            this.lblFormPagmento.BackColor = System.Drawing.Color.Transparent;
            this.lblFormPagmento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFormPagmento.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormPagmento.ForeColor = System.Drawing.Color.Black;
            this.lblFormPagmento.Location = new System.Drawing.Point(21, 223);
            this.lblFormPagmento.Name = "lblFormPagmento";
            this.lblFormPagmento.Size = new System.Drawing.Size(272, 27);
            this.lblFormPagmento.TabIndex = 2;
            this.lblFormPagmento.Text = "Forma de Pagamento";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.BackColor = System.Drawing.Color.Transparent;
            this.lblCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblCliente.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.ForeColor = System.Drawing.Color.Black;
            this.lblCliente.Location = new System.Drawing.Point(21, 311);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(95, 27);
            this.lblCliente.TabIndex = 3;
            this.lblCliente.Text = "Cliente";
            this.lblCliente.Click += new System.EventHandler(this.lblCliente_Click);
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.AutoSize = true;
            this.lblQuantidade.BackColor = System.Drawing.Color.Transparent;
            this.lblQuantidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblQuantidade.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.ForeColor = System.Drawing.Color.Black;
            this.lblQuantidade.Location = new System.Drawing.Point(191, 97);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(153, 27);
            this.lblQuantidade.TabIndex = 4;
            this.lblQuantidade.Text = "Quantidade";
            // 
            // lstFormadePgmt
            // 
            this.lstFormadePgmt.FormattingEnabled = true;
            this.lstFormadePgmt.Items.AddRange(new object[] {
            "Débito",
            "Crédito",
            "Dinheiro"});
            this.lstFormadePgmt.Location = new System.Drawing.Point(26, 253);
            this.lstFormadePgmt.Name = "lstFormadePgmt";
            this.lstFormadePgmt.Size = new System.Drawing.Size(95, 30);
            this.lstFormadePgmt.TabIndex = 7;
            this.lstFormadePgmt.SelectedIndexChanged += new System.EventHandler(this.lstFormadePgmt_SelectedIndexChanged);
            // 
            // btnrealizarpedido
            // 
            this.btnrealizarpedido.BackColor = System.Drawing.Color.Transparent;
            this.btnrealizarpedido.Image = global::Dona_Cherry.Properties.Resources.Adicionar_ao_carrinho;
            this.btnrealizarpedido.Location = new System.Drawing.Point(344, 21);
            this.btnrealizarpedido.Name = "btnrealizarpedido";
            this.btnrealizarpedido.Size = new System.Drawing.Size(86, 60);
            this.btnrealizarpedido.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnrealizarpedido.TabIndex = 11;
            this.btnrealizarpedido.TabStop = false;
            this.btnrealizarpedido.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Transparent;
            this.btnVoltar.Image = global::Dona_Cherry.Properties.Resources.VOLTAR;
            this.btnVoltar.Location = new System.Drawing.Point(638, 341);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(44, 42);
            this.btnVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnVoltar.TabIndex = 57;
            this.btnVoltar.TabStop = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // lblcompra
            // 
            this.lblcompra.AutoSize = true;
            this.lblcompra.BackColor = System.Drawing.Color.Transparent;
            this.lblcompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcompra.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcompra.ForeColor = System.Drawing.Color.Black;
            this.lblcompra.Location = new System.Drawing.Point(288, 169);
            this.lblcompra.Name = "lblcompra";
            this.lblcompra.Size = new System.Drawing.Size(64, 27);
            this.lblcompra.TabIndex = 60;
            this.lblcompra.Text = "0,00";
            this.lblcompra.Click += new System.EventHandler(this.lblcompra_Click);
            // 
            // btnTotal
            // 
            this.btnTotal.BackColor = System.Drawing.Color.MistyRose;
            this.btnTotal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotal.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotal.ForeColor = System.Drawing.Color.Black;
            this.btnTotal.Location = new System.Drawing.Point(196, 165);
            this.btnTotal.Name = "btnTotal";
            this.btnTotal.Size = new System.Drawing.Size(86, 37);
            this.btnTotal.TabIndex = 63;
            this.btnTotal.Text = "Total";
            this.btnTotal.UseVisualStyleBackColor = false;
            this.btnTotal.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvcarrinho
            // 
            this.dgvcarrinho.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dgvcarrinho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvcarrinho.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Produtos});
            this.dgvcarrinho.Location = new System.Drawing.Point(436, 19);
            this.dgvcarrinho.Name = "dgvcarrinho";
            this.dgvcarrinho.Size = new System.Drawing.Size(240, 150);
            this.dgvcarrinho.TabIndex = 64;
            // 
            // Produtos
            // 
            this.Produtos.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Produtos.DataPropertyName = "Nome";
            this.Produtos.HeaderText = "Produtos";
            this.Produtos.Name = "Produtos";
            // 
            // cboproduto
            // 
            this.cboproduto.FormattingEnabled = true;
            this.cboproduto.Location = new System.Drawing.Point(198, 51);
            this.cboproduto.Name = "cboproduto";
            this.cboproduto.Size = new System.Drawing.Size(121, 21);
            this.cboproduto.TabIndex = 68;
            this.cboproduto.SelectedIndexChanged += new System.EventHandler(this.cboproduto_SelectedIndexChanged);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(199, 127);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(86, 20);
            this.numericUpDown1.TabIndex = 69;
            // 
            // btnemitirpedido
            // 
            this.btnemitirpedido.BackColor = System.Drawing.Color.Transparent;
            this.btnemitirpedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnemitirpedido.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Bold);
            this.btnemitirpedido.ForeColor = System.Drawing.Color.Black;
            this.btnemitirpedido.Location = new System.Drawing.Point(462, 239);
            this.btnemitirpedido.Name = "btnemitirpedido";
            this.btnemitirpedido.Size = new System.Drawing.Size(202, 44);
            this.btnemitirpedido.TabIndex = 70;
            this.btnemitirpedido.Text = "Emitir Pedido";
            this.btnemitirpedido.UseVisualStyleBackColor = false;
            this.btnemitirpedido.Click += new System.EventHandler(this.btnemitirpedido_Click);
            // 
            // imgProduto
            // 
            this.imgProduto.BackColor = System.Drawing.Color.MistyRose;
            this.imgProduto.Location = new System.Drawing.Point(26, 21);
            this.imgProduto.Name = "imgProduto";
            this.imgProduto.Size = new System.Drawing.Size(159, 181);
            this.imgProduto.TabIndex = 72;
            this.imgProduto.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.cbocliente);
            this.groupBox1.Controls.Add(this.imgProduto);
            this.groupBox1.Controls.Add(this.btnVoltar);
            this.groupBox1.Controls.Add(this.lblProduto);
            this.groupBox1.Controls.Add(this.btnemitirpedido);
            this.groupBox1.Controls.Add(this.lblCliente);
            this.groupBox1.Controls.Add(this.btnrealizarpedido);
            this.groupBox1.Controls.Add(this.cboproduto);
            this.groupBox1.Controls.Add(this.lstFormadePgmt);
            this.groupBox1.Controls.Add(this.dgvcarrinho);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.lblFormPagmento);
            this.groupBox1.Controls.Add(this.lblcompra);
            this.groupBox1.Controls.Add(this.btnTotal);
            this.groupBox1.Controls.Add(this.lblQuantidade);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(89, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(682, 383);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            // 
            // cbocliente
            // 
            this.cbocliente.FormattingEnabled = true;
            this.cbocliente.Location = new System.Drawing.Point(26, 341);
            this.cbocliente.Name = "cbocliente";
            this.cbocliente.Size = new System.Drawing.Size(198, 21);
            this.cbocliente.TabIndex = 73;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Dona_Cherry.Properties.Resources.Flourish_Monogram_10;
            this.pictureBox1.Location = new System.Drawing.Point(462, 192);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(202, 130);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 74;
            this.pictureBox1.TabStop = false;
            // 
            // RealizarPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Dona_Cherry.Properties.Resources.babypink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(786, 413);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RealizarPedido";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Realizar Pedido";
            this.Load += new System.EventHandler(this.RealizarPedido_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnrealizarpedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVoltar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcarrinho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblProduto;
        private System.Windows.Forms.Label lblFormPagmento;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lblQuantidade;
        private System.Windows.Forms.ListBox lstFormadePgmt;
        private System.Windows.Forms.PictureBox btnrealizarpedido;
        private System.Windows.Forms.PictureBox btnVoltar;
        private System.Windows.Forms.Label lblcompra;
        private System.Windows.Forms.Button btnTotal;
        private System.Windows.Forms.DataGridView dgvcarrinho;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produtos;
        private System.Windows.Forms.ComboBox cboproduto;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button btnemitirpedido;
        private System.Windows.Forms.PictureBox imgProduto;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbocliente;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}