﻿using Dona_Cherry.DB.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.Estoque
{
    public partial class ConsultaEstoque : Form
    {
        public ConsultaEstoque()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry tela = new FrmDonaCherry();
            tela.Show();
            this.Hide();

        }

        private void ConsultaEstoque_Load(object sender, EventArgs e)
        {

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                EstoqueBusiness business = new EstoqueBusiness();
                List<EstoqueDTO> dto = business.Consultar(textBox1.Text);

                dgvConsultarEstoque.AutoGenerateColumns = false;
                dgvConsultarEstoque.DataSource = dto;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnatualizar_Click(object sender, EventArgs e)
        {
            CadastroEstoque novo = new CadastroEstoque();
            novo.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                EstoqueBusiness business = new EstoqueBusiness();
                List<EstoqueDTO> dto = business.Listar();

                dgvConsultarEstoque.AutoGenerateColumns = false;
                dgvConsultarEstoque.DataSource = dto;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnremover_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarEstoque.CurrentRow != null)
                {
                    EstoqueDTO estoque = dgvConsultarEstoque.CurrentRow.DataBoundItem as EstoqueDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse item?", "Dona Cherry",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        EstoqueBusiness business = new EstoqueBusiness();
                        business.Remover(estoque.id_produto);

                        List<EstoqueDTO> a = business.Listar();
                        dgvConsultarEstoque.AutoGenerateColumns = false;
                        dgvConsultarEstoque.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um item");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
