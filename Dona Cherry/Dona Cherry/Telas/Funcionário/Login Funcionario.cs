﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.Funcionário
{
    public partial class Login_Funcionario : Form
    {
        public Login_Funcionario()
        {
            InitializeComponent();
        }

        private void Login_Funcionario_Load(object sender, EventArgs e)
        {

        }

        private void lblNome_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }
        
        private void imgSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtLogin.Text, txtsenha.Text);

                if (funcionario != null)
                {
                    UserSession.UsuarioLogado = funcionario;

                    FrmDonaCherry DonaCherry = new FrmDonaCherry();
                    DonaCherry.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Dona Cherry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Splash voltar = new Splash();
            voltar.Show();
            this.Hide();
        }

        private void txtsenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblPodeEntrar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //CadastroAcesso cd = new CadastroAcesso();
            //cd.Show();
            //this.Hide();
        }

        private void lblPodeEntrar_Click(object sender, EventArgs e)
        {
            //CadastroAcesso cd = new CadastroAcesso();
            //cd.Show();
            //this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
