﻿namespace Dona_Cherry.Telas.Funcionário
{
    partial class Alterar_Funcionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnome = new System.Windows.Forms.Label();
            this.lblemail = new System.Windows.Forms.Label();
            this.lbltelefone = new System.Windows.Forms.Label();
            this.lblcomplemento = new System.Windows.Forms.Label();
            this.lblcelular = new System.Windows.Forms.Label();
            this.lbldatadeadmissao = new System.Windows.Forms.Label();
            this.lblcep = new System.Windows.Forms.Label();
            this.lblrg = new System.Windows.Forms.Label();
            this.lblrua = new System.Windows.Forms.Label();
            this.lblbairro = new System.Windows.Forms.Label();
            this.lblcpf = new System.Windows.Forms.Label();
            this.lblcargo = new System.Windows.Forms.Label();
            this.cidade = new System.Windows.Forms.Label();
            this.lblsalario = new System.Windows.Forms.Label();
            this.lblestado = new System.Windows.Forms.Label();
            this.lblcargahoraria = new System.Windows.Forms.Label();
            this.gpbpermissoes = new System.Windows.Forms.GroupBox();
            this.rdnFuncionario = new System.Windows.Forms.RadioButton();
            this.rdnRH = new System.Windows.Forms.RadioButton();
            this.rdnFinanceiro = new System.Windows.Forms.RadioButton();
            this.gbplogin = new System.Windows.Forms.GroupBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.lblsenha = new System.Windows.Forms.Label();
            this.lblusuario = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.txtCargo = new System.Windows.Forms.TextBox();
            this.txtSalario = new System.Windows.Forms.TextBox();
            this.dtpDatadeAdmissao = new System.Windows.Forms.DateTimePicker();
            this.lstestado = new System.Windows.Forms.ListBox();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.txtrg = new System.Windows.Forms.MaskedTextBox();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.gpbpermissoes.SuspendLayout();
            this.gbplogin.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.BackColor = System.Drawing.Color.Transparent;
            this.lblnome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblnome.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnome.Location = new System.Drawing.Point(10, 20);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(70, 24);
            this.lblnome.TabIndex = 1;
            this.lblnome.Text = "Nome";
            // 
            // lblemail
            // 
            this.lblemail.AutoSize = true;
            this.lblemail.BackColor = System.Drawing.Color.Transparent;
            this.lblemail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblemail.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemail.Location = new System.Drawing.Point(3, 56);
            this.lblemail.Name = "lblemail";
            this.lblemail.Size = new System.Drawing.Size(75, 24);
            this.lblemail.TabIndex = 5;
            this.lblemail.Text = "E-mail";
            // 
            // lbltelefone
            // 
            this.lbltelefone.AutoSize = true;
            this.lbltelefone.BackColor = System.Drawing.Color.Transparent;
            this.lbltelefone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltelefone.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltelefone.Location = new System.Drawing.Point(367, 19);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(93, 24);
            this.lbltelefone.TabIndex = 6;
            this.lbltelefone.Text = "Telefone";
            // 
            // lblcomplemento
            // 
            this.lblcomplemento.AutoSize = true;
            this.lblcomplemento.BackColor = System.Drawing.Color.Transparent;
            this.lblcomplemento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcomplemento.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcomplemento.Location = new System.Drawing.Point(1, 232);
            this.lblcomplemento.Name = "lblcomplemento";
            this.lblcomplemento.Size = new System.Drawing.Size(145, 24);
            this.lblcomplemento.TabIndex = 16;
            this.lblcomplemento.Text = "Complemento";
            // 
            // lblcelular
            // 
            this.lblcelular.AutoSize = true;
            this.lblcelular.BackColor = System.Drawing.Color.Transparent;
            this.lblcelular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcelular.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcelular.Location = new System.Drawing.Point(378, 56);
            this.lblcelular.Name = "lblcelular";
            this.lblcelular.Size = new System.Drawing.Size(82, 24);
            this.lblcelular.TabIndex = 17;
            this.lblcelular.Text = "Celular";
            // 
            // lbldatadeadmissao
            // 
            this.lbldatadeadmissao.AutoSize = true;
            this.lbldatadeadmissao.BackColor = System.Drawing.Color.Transparent;
            this.lbldatadeadmissao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbldatadeadmissao.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldatadeadmissao.Location = new System.Drawing.Point(172, 266);
            this.lbldatadeadmissao.Name = "lbldatadeadmissao";
            this.lbldatadeadmissao.Size = new System.Drawing.Size(198, 24);
            this.lbldatadeadmissao.TabIndex = 18;
            this.lbldatadeadmissao.Text = "Data de Admissão";
            // 
            // lblcep
            // 
            this.lblcep.AutoSize = true;
            this.lblcep.BackColor = System.Drawing.Color.Transparent;
            this.lblcep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcep.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcep.Location = new System.Drawing.Point(33, 132);
            this.lblcep.Name = "lblcep";
            this.lblcep.Size = new System.Drawing.Size(52, 24);
            this.lblcep.TabIndex = 19;
            this.lblcep.Text = "CEP";
            this.lblcep.Click += new System.EventHandler(this.lblcep_Click);
            // 
            // lblrg
            // 
            this.lblrg.AutoSize = true;
            this.lblrg.BackColor = System.Drawing.Color.Transparent;
            this.lblrg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblrg.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrg.Location = new System.Drawing.Point(419, 94);
            this.lblrg.Name = "lblrg";
            this.lblrg.Size = new System.Drawing.Size(41, 24);
            this.lblrg.TabIndex = 20;
            this.lblrg.Text = "RG";
            // 
            // lblrua
            // 
            this.lblrua.AutoSize = true;
            this.lblrua.BackColor = System.Drawing.Color.Transparent;
            this.lblrua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblrua.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrua.Location = new System.Drawing.Point(1, 98);
            this.lblrua.Name = "lblrua";
            this.lblrua.Size = new System.Drawing.Size(103, 24);
            this.lblrua.TabIndex = 21;
            this.lblrua.Text = "Endereço";
            // 
            // lblbairro
            // 
            this.lblbairro.AutoSize = true;
            this.lblbairro.BackColor = System.Drawing.Color.Transparent;
            this.lblbairro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblbairro.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbairro.Location = new System.Drawing.Point(6, 164);
            this.lblbairro.Name = "lblbairro";
            this.lblbairro.Size = new System.Drawing.Size(78, 24);
            this.lblbairro.TabIndex = 22;
            this.lblbairro.Text = "Bairro";
            this.lblbairro.Click += new System.EventHandler(this.lblbairro_Click);
            // 
            // lblcpf
            // 
            this.lblcpf.AutoSize = true;
            this.lblcpf.BackColor = System.Drawing.Color.Transparent;
            this.lblcpf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcpf.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpf.Location = new System.Drawing.Point(407, 132);
            this.lblcpf.Name = "lblcpf";
            this.lblcpf.Size = new System.Drawing.Size(53, 24);
            this.lblcpf.TabIndex = 23;
            this.lblcpf.Text = "CPF";
            // 
            // lblcargo
            // 
            this.lblcargo.AutoSize = true;
            this.lblcargo.BackColor = System.Drawing.Color.Transparent;
            this.lblcargo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcargo.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcargo.Location = new System.Drawing.Point(390, 164);
            this.lblcargo.Name = "lblcargo";
            this.lblcargo.Size = new System.Drawing.Size(70, 24);
            this.lblcargo.TabIndex = 24;
            this.lblcargo.Text = "Cargo";
            // 
            // cidade
            // 
            this.cidade.AutoSize = true;
            this.cidade.BackColor = System.Drawing.Color.Transparent;
            this.cidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cidade.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cidade.Location = new System.Drawing.Point(6, 198);
            this.cidade.Name = "cidade";
            this.cidade.Size = new System.Drawing.Size(79, 24);
            this.cidade.TabIndex = 25;
            this.cidade.Text = "Cidade";
            // 
            // lblsalario
            // 
            this.lblsalario.AutoSize = true;
            this.lblsalario.BackColor = System.Drawing.Color.Transparent;
            this.lblsalario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblsalario.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsalario.Location = new System.Drawing.Point(378, 198);
            this.lblsalario.Name = "lblsalario";
            this.lblsalario.Size = new System.Drawing.Size(82, 24);
            this.lblsalario.TabIndex = 26;
            this.lblsalario.Text = "Salário";
            // 
            // lblestado
            // 
            this.lblestado.AutoSize = true;
            this.lblestado.BackColor = System.Drawing.Color.Transparent;
            this.lblestado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblestado.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblestado.Location = new System.Drawing.Point(6, 273);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(79, 24);
            this.lblestado.TabIndex = 27;
            this.lblestado.Text = "Estado";
            // 
            // lblcargahoraria
            // 
            this.lblcargahoraria.AutoSize = true;
            this.lblcargahoraria.BackColor = System.Drawing.Color.Transparent;
            this.lblcargahoraria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcargahoraria.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcargahoraria.Location = new System.Drawing.Point(378, 232);
            this.lblcargahoraria.Name = "lblcargahoraria";
            this.lblcargahoraria.Size = new System.Drawing.Size(164, 24);
            this.lblcargahoraria.TabIndex = 44;
            this.lblcargahoraria.Text = "Carga Horária";
            // 
            // gpbpermissoes
            // 
            this.gpbpermissoes.BackColor = System.Drawing.Color.Transparent;
            this.gpbpermissoes.Controls.Add(this.rdnFuncionario);
            this.gpbpermissoes.Controls.Add(this.rdnRH);
            this.gpbpermissoes.Controls.Add(this.rdnFinanceiro);
            this.gpbpermissoes.Font = new System.Drawing.Font("Lucida Calligraphy", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbpermissoes.Location = new System.Drawing.Point(319, 336);
            this.gpbpermissoes.Name = "gpbpermissoes";
            this.gpbpermissoes.Size = new System.Drawing.Size(207, 149);
            this.gpbpermissoes.TabIndex = 45;
            this.gpbpermissoes.TabStop = false;
            this.gpbpermissoes.Text = "Permissões";
            // 
            // rdnFuncionario
            // 
            this.rdnFuncionario.AutoSize = true;
            this.rdnFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.rdnFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdnFuncionario.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnFuncionario.Location = new System.Drawing.Point(0, 103);
            this.rdnFuncionario.Name = "rdnFuncionario";
            this.rdnFuncionario.Size = new System.Drawing.Size(174, 31);
            this.rdnFuncionario.TabIndex = 34;
            this.rdnFuncionario.TabStop = true;
            this.rdnFuncionario.Text = "Funcionário ";
            this.rdnFuncionario.UseVisualStyleBackColor = false;
            // 
            // rdnRH
            // 
            this.rdnRH.AutoSize = true;
            this.rdnRH.BackColor = System.Drawing.Color.Transparent;
            this.rdnRH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdnRH.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnRH.Location = new System.Drawing.Point(0, 32);
            this.rdnRH.Name = "rdnRH";
            this.rdnRH.Size = new System.Drawing.Size(69, 31);
            this.rdnRH.TabIndex = 32;
            this.rdnRH.TabStop = true;
            this.rdnRH.Text = "RH";
            this.rdnRH.UseVisualStyleBackColor = false;
            // 
            // rdnFinanceiro
            // 
            this.rdnFinanceiro.AutoSize = true;
            this.rdnFinanceiro.BackColor = System.Drawing.Color.Transparent;
            this.rdnFinanceiro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdnFinanceiro.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnFinanceiro.Location = new System.Drawing.Point(0, 66);
            this.rdnFinanceiro.Name = "rdnFinanceiro";
            this.rdnFinanceiro.Size = new System.Drawing.Size(151, 31);
            this.rdnFinanceiro.TabIndex = 33;
            this.rdnFinanceiro.TabStop = true;
            this.rdnFinanceiro.Text = "Financeiro";
            this.rdnFinanceiro.UseVisualStyleBackColor = false;
            // 
            // gbplogin
            // 
            this.gbplogin.BackColor = System.Drawing.Color.Transparent;
            this.gbplogin.Controls.Add(this.txtsenha);
            this.gbplogin.Controls.Add(this.txtusuario);
            this.gbplogin.Controls.Add(this.lblsenha);
            this.gbplogin.Controls.Add(this.lblusuario);
            this.gbplogin.Font = new System.Drawing.Font("Lucida Calligraphy", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbplogin.Location = new System.Drawing.Point(96, 336);
            this.gbplogin.Name = "gbplogin";
            this.gbplogin.Size = new System.Drawing.Size(208, 149);
            this.gbplogin.TabIndex = 46;
            this.gbplogin.TabStop = false;
            this.gbplogin.Text = "Login";
            this.gbplogin.Enter += new System.EventHandler(this.gbplogin_Enter);
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(79, 71);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(113, 25);
            this.txtsenha.TabIndex = 45;
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(79, 24);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(113, 25);
            this.txtusuario.TabIndex = 44;
            // 
            // lblsenha
            // 
            this.lblsenha.AutoSize = true;
            this.lblsenha.BackColor = System.Drawing.Color.Transparent;
            this.lblsenha.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F);
            this.lblsenha.Location = new System.Drawing.Point(5, 72);
            this.lblsenha.Name = "lblsenha";
            this.lblsenha.Size = new System.Drawing.Size(76, 24);
            this.lblsenha.TabIndex = 43;
            this.lblsenha.Text = "Senha:";
            // 
            // lblusuario
            // 
            this.lblusuario.AutoSize = true;
            this.lblusuario.BackColor = System.Drawing.Color.Transparent;
            this.lblusuario.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F);
            this.lblusuario.Location = new System.Drawing.Point(6, 25);
            this.lblusuario.Name = "lblusuario";
            this.lblusuario.Size = new System.Drawing.Size(75, 24);
            this.lblusuario.TabIndex = 42;
            this.lblusuario.Text = "Nome:";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(86, 19);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(188, 20);
            this.txtnome.TabIndex = 47;
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(87, 60);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(187, 20);
            this.txtemail.TabIndex = 48;
            // 
            // txtCEP
            // 
            this.txtCEP.Location = new System.Drawing.Point(87, 132);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(63, 20);
            this.txtCEP.TabIndex = 49;
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(87, 164);
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(190, 20);
            this.txtbairro.TabIndex = 50;
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(86, 202);
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(190, 20);
            this.txtcidade.TabIndex = 51;
            this.txtcidade.TextChanged += new System.EventHandler(this.txtcidade_TextChanged);
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(149, 236);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(200, 20);
            this.txtComplemento.TabIndex = 55;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(109, 98);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(240, 20);
            this.txtEndereco.TabIndex = 56;
            // 
            // txtCargo
            // 
            this.txtCargo.Location = new System.Drawing.Point(466, 164);
            this.txtCargo.Name = "txtCargo";
            this.txtCargo.Size = new System.Drawing.Size(135, 20);
            this.txtCargo.TabIndex = 57;
            this.txtCargo.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // txtSalario
            // 
            this.txtSalario.Location = new System.Drawing.Point(466, 198);
            this.txtSalario.Name = "txtSalario";
            this.txtSalario.Size = new System.Drawing.Size(135, 20);
            this.txtSalario.TabIndex = 60;
            // 
            // dtpDatadeAdmissao
            // 
            this.dtpDatadeAdmissao.Location = new System.Drawing.Point(373, 267);
            this.dtpDatadeAdmissao.Name = "dtpDatadeAdmissao";
            this.dtpDatadeAdmissao.Size = new System.Drawing.Size(228, 20);
            this.dtpDatadeAdmissao.TabIndex = 63;
            // 
            // lstestado
            // 
            this.lstestado.FormattingEnabled = true;
            this.lstestado.Items.AddRange(new object[] {
            "SP",
            "RJ",
            "SC",
            "AM",
            "MG",
            "AC",
            "AL",
            "AP",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RN",
            "RS",
            "RO",
            "RR",
            "SE",
            "TO"});
            this.lstestado.Location = new System.Drawing.Point(86, 267);
            this.lstestado.Name = "lstestado";
            this.lstestado.Size = new System.Drawing.Size(64, 30);
            this.lstestado.TabIndex = 64;
            // 
            // txttelefone
            // 
            this.txttelefone.Location = new System.Drawing.Point(466, 20);
            this.txttelefone.Mask = "(999) 000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(89, 20);
            this.txttelefone.TabIndex = 65;
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(466, 56);
            this.maskedTextBox1.Mask = "(999) 000-0000";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(89, 20);
            this.maskedTextBox1.TabIndex = 66;
            // 
            // txtrg
            // 
            this.txtrg.Location = new System.Drawing.Point(466, 94);
            this.txtrg.Mask = "99 000-0000 - 0";
            this.txtrg.Name = "txtrg";
            this.txtrg.Size = new System.Drawing.Size(89, 20);
            this.txtrg.TabIndex = 67;
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(466, 136);
            this.txtcpf.Mask = "999 000-0000/00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(89, 20);
            this.txtcpf.TabIndex = 68;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblnome);
            this.groupBox1.Controls.Add(this.dtpDatadeAdmissao);
            this.groupBox1.Controls.Add(this.txtcpf);
            this.groupBox1.Controls.Add(this.txtSalario);
            this.groupBox1.Controls.Add(this.txtnome);
            this.groupBox1.Controls.Add(this.txtCargo);
            this.groupBox1.Controls.Add(this.lbldatadeadmissao);
            this.groupBox1.Controls.Add(this.lblcargahoraria);
            this.groupBox1.Controls.Add(this.txtrg);
            this.groupBox1.Controls.Add(this.lblemail);
            this.groupBox1.Controls.Add(this.maskedTextBox1);
            this.groupBox1.Controls.Add(this.lblsalario);
            this.groupBox1.Controls.Add(this.txtemail);
            this.groupBox1.Controls.Add(this.txttelefone);
            this.groupBox1.Controls.Add(this.lblcep);
            this.groupBox1.Controls.Add(this.lblcargo);
            this.groupBox1.Controls.Add(this.lstestado);
            this.groupBox1.Controls.Add(this.txtCEP);
            this.groupBox1.Controls.Add(this.lblbairro);
            this.groupBox1.Controls.Add(this.lblcpf);
            this.groupBox1.Controls.Add(this.txtbairro);
            this.groupBox1.Controls.Add(this.txtcidade);
            this.groupBox1.Controls.Add(this.txtEndereco);
            this.groupBox1.Controls.Add(this.lblrg);
            this.groupBox1.Controls.Add(this.cidade);
            this.groupBox1.Controls.Add(this.txtComplemento);
            this.groupBox1.Controls.Add(this.lblcomplemento);
            this.groupBox1.Controls.Add(this.lblestado);
            this.groupBox1.Controls.Add(this.lblcelular);
            this.groupBox1.Controls.Add(this.lblrua);
            this.groupBox1.Controls.Add(this.lbltelefone);
            this.groupBox1.Location = new System.Drawing.Point(96, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(612, 307);
            this.groupBox1.TabIndex = 69;
            this.groupBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(562, 401);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 31);
            this.button1.TabIndex = 70;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Alterar_Funcionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Dona_Cherry.Properties.Resources.babypink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(740, 497);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbplogin);
            this.Controls.Add(this.gpbpermissoes);
            this.Name = "Alterar_Funcionario";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar_Funcionario";
            this.Load += new System.EventHandler(this.Alterar_Funcionario_Load);
            this.gpbpermissoes.ResumeLayout(false);
            this.gpbpermissoes.PerformLayout();
            this.gbplogin.ResumeLayout(false);
            this.gbplogin.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.Label lblemail;
        private System.Windows.Forms.Label lbltelefone;
        private System.Windows.Forms.Label lblcomplemento;
        private System.Windows.Forms.Label lblcelular;
        private System.Windows.Forms.Label lbldatadeadmissao;
        private System.Windows.Forms.Label lblcep;
        private System.Windows.Forms.Label lblrg;
        private System.Windows.Forms.Label lblrua;
        private System.Windows.Forms.Label lblbairro;
        private System.Windows.Forms.Label lblcpf;
        private System.Windows.Forms.Label lblcargo;
        private System.Windows.Forms.Label cidade;
        private System.Windows.Forms.Label lblsalario;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.Label lblcargahoraria;
        private System.Windows.Forms.GroupBox gpbpermissoes;
        private System.Windows.Forms.RadioButton rdnFuncionario;
        private System.Windows.Forms.RadioButton rdnRH;
        private System.Windows.Forms.RadioButton rdnFinanceiro;
        private System.Windows.Forms.GroupBox gbplogin;
        private System.Windows.Forms.Label lblusuario;
        private System.Windows.Forms.Label lblsenha;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.TextBox txtCargo;
        private System.Windows.Forms.TextBox txtSalario;
        private System.Windows.Forms.DateTimePicker dtpDatadeAdmissao;
        private System.Windows.Forms.ListBox lstestado;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.MaskedTextBox txtrg;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}