﻿using Dona_Cherry.Telas.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry
{
    public partial class Consultar_Funcionário : Form
    {
        public Consultar_Funcionário()
        {
            InitializeComponent();
        }

        private void Consultar_Funcionário_Load(object sender, EventArgs e)
        {
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                List<FuncionarioDTO> a = business.Consultar(txtFuncionario.Text);
                dgvConsultarFuncionario.AutoGenerateColumns = false;
                dgvConsultarFuncionario.DataSource = a;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            



        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry voltar = new FrmDonaCherry();
            voltar.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {


                FuncionarioDTO dto = new FuncionarioDTO();
                dto.nome = txtFuncionario.Text;


            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro " + ex.Message);
            }



        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarFuncionario.CurrentRow != null)
                {
                    FuncionarioDTO funcionario = dgvConsultarFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;
                    DialogResult r = MessageBox.Show("Deseja remover esse funcionário?", "Dona Cherry",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);

                    if (funcionario.id_funcionario != UserSession.UsuarioLogado.id_funcionario)
                    {

                        if (r == DialogResult.Yes)
                        {
                            FuncionarioBusiness business = new FuncionarioBusiness();
                            business.Remover(funcionario.id_funcionario);

                            List<FuncionarioDTO> a = business.Consultar(txtFuncionario.Text);
                            dgvConsultarFuncionario.AutoGenerateColumns = false;
                            dgvConsultarFuncionario.DataSource = a;

                        }
                    }

                    else
                    {
                        MessageBox.Show("Não é possível deletar um usuário que está logado");
                    }
                }
                else
                {
                    MessageBox.Show("Selecione um funcionario");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

        }


        private void cbConsultarFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {




        }

        private void txtFuncionario_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {

        }

        private void btnRemover_Click(object sender, EventArgs e)
        {

        }

        private void btnRemover_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarFuncionario.CurrentRow != null)
                {
                    FuncionarioDTO funcionario = dgvConsultarFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse funcionário?", "Dona Cherry",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        FuncionarioBusiness business = new FuncionarioBusiness();
                        business.Remover(funcionario.id_funcionario);

                        List<FuncionarioDTO> a = business.ListarPerfilFuncionario();
                        dgvConsultarFuncionario.AutoGenerateColumns = false;
                        dgvConsultarFuncionario.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um Funcionário");
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void btnAlterar_Click_1(object sender, EventArgs e)
        {
            Alterar_Funcionario alterar = new Alterar_Funcionario();
            alterar.Show();
            this.Hide();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

            if (dgvConsultarFuncionario.CurrentRow != null)
            {
                FuncionarioDTO funcionario = dgvConsultarFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;
                DialogResult r = MessageBox.Show("Deseja excluir esse funcionário?", "Dona Cherry",
                                       MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(funcionario.id_funcionario);

                    List<FuncionarioDTO> a = business.ListarPerfilFuncionario();
                    dgvConsultarFuncionario.AutoGenerateColumns = false;
                    dgvConsultarFuncionario.DataSource = a;

                }
                else
                {
                    MessageBox.Show("Selecione um Funcionário");
                }
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Alterar_Funcionario alterar = new Alterar_Funcionario();
            alterar.Show();
            this.Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FuncionarioBusiness bus = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = bus.ListarPerfilFuncionario();

            dgvConsultarFuncionario.AutoGenerateColumns = false;
            dgvConsultarFuncionario.DataSource = lista;
        }

        private void dgvConsultarFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}