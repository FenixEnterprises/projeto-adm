﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.Funcionário
{
    public partial class Alterar_Funcionario : Form
    {
        public Alterar_Funcionario()
        {
            InitializeComponent();
        }

        private void Alterar_Funcionario_Load(object sender, EventArgs e)
        {

        }

        private void lblcep_Click(object sender, EventArgs e)
        {

        }

        private void lblbairro_Click(object sender, EventArgs e)
        {

        }

        private void gbplogin_Enter(object sender, EventArgs e)
        {

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                //AcessoDTO finan = rdnFinanceiro.Checked as FuncionarioDTO;
                //AcessoDTO funcio = rdnFuncionario.Checked as FuncionarioDTO;
                //AcessoDTO rh = rdnRH.Checked as FuncionarioDTO;


                //int alterar = this.dto.id;

                FuncionarioDTO dto = new FuncionarioDTO();
                //dto.id = funcio;

                dto.nome = txtnome.Text;
                dto.bairro = txtbairro.Text;
                dto.RG = txtrg.Text;
                dto.salario = txtSalario.Text;
                dto.CPF = txtcpf.Text;
                dto.telefonefixo = txttelefone.Text;
                dto.email = txtemail.Text;
                dto.cidade = txtcidade.Text;
                dto.estado = lstestado.Text ;
                dto.cep = txtCEP.Text;
                dto.dataadmissao = dtpDatadeAdmissao.Value;
                dto.permissaofinanceiro = rdnFinanceiro.Checked;
                dto.permissaofuncionario = rdnFuncionario.Checked;
                dto.permissaorh = rdnRH.Checked;
                dto.usuario = txtusuario.Text;
                dto.senha = txtsenha.Text;

                FuncionarioBusiness buss = new FuncionarioBusiness();
                buss.Alterar(dto);

                MessageBox.Show("Funcionário alterado com suceso!!", "Dona Cherry", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtcidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //AcessoDTO finan = rdnFinanceiro.Checked as FuncionarioDTO;
                //AcessoDTO funcio = rdnFuncionario.Checked as FuncionarioDTO;
                //AcessoDTO rh = rdnRH.Checked as FuncionarioDTO;


                //int alterar = this.dto.id;

                FuncionarioDTO dto = new FuncionarioDTO();
                //dto.id = funcio;

                dto.nome = txtnome.Text;
                dto.bairro = txtbairro.Text;
                dto.RG = txtrg.Text;
                dto.salario = txtSalario.Text;
                dto.CPF = txtcpf.Text;
                dto.telefonefixo = txttelefone.Text;
                dto.email = txtemail.Text;
                dto.cidade = txtcidade.Text;
                dto.estado = lstestado.Text;
                dto.cep = txtCEP.Text;
                dto.dataadmissao = dtpDatadeAdmissao.Value;
                dto.permissaofinanceiro = rdnFinanceiro.Checked;
                dto.permissaofuncionario = rdnFuncionario.Checked;
                dto.permissaorh = rdnRH.Checked;
                dto.usuario = txtusuario.Text;
                dto.senha = txtsenha.Text;

                FuncionarioBusiness buss = new FuncionarioBusiness();
                buss.Alterar(dto);

                MessageBox.Show("Funcionário alterado com suceso!!", "Dona Cherry", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
