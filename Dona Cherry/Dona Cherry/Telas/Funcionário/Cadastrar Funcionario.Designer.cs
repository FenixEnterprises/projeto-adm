﻿namespace Dona_Cherry.Telas.Funcionário
{
    partial class Cadastrar_Funcionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnome = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.lbltelefone = new System.Windows.Forms.Label();
            this.lblcelular = new System.Windows.Forms.Label();
            this.lblemail = new System.Windows.Forms.Label();
            this.lblrg = new System.Windows.Forms.Label();
            this.lblcep = new System.Windows.Forms.Label();
            this.lblcpf = new System.Windows.Forms.Label();
            this.lblrua = new System.Windows.Forms.Label();
            this.lblcargo = new System.Windows.Forms.Label();
            this.lblbairro = new System.Windows.Forms.Label();
            this.cidade = new System.Windows.Forms.Label();
            this.lblsalario = new System.Windows.Forms.Label();
            this.lblestado = new System.Windows.Forms.Label();
            this.lblcargahoraria = new System.Windows.Forms.Label();
            this.lblcomplemento = new System.Windows.Forms.Label();
            this.lbldatadeadmissao = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtendereco = new System.Windows.Forms.TextBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.txtsalario = new System.Windows.Forms.TextBox();
            this.txtcargahoraria = new System.Windows.Forms.TextBox();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.txtcargo = new System.Windows.Forms.TextBox();
            this.dtpdataadmissao = new System.Windows.Forms.DateTimePicker();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.txtrg = new System.Windows.Forms.MaskedTextBox();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.txtcep = new System.Windows.Forms.MaskedTextBox();
            this.rdnRH = new System.Windows.Forms.RadioButton();
            this.rdnFinanceiro = new System.Windows.Forms.RadioButton();
            this.rdnFuncionario = new System.Windows.Forms.RadioButton();
            this.gpbpermissoes = new System.Windows.Forms.GroupBox();
            this.btnvoltar = new System.Windows.Forms.PictureBox();
            this.lstestado = new System.Windows.Forms.ListBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.lblsenha = new System.Windows.Forms.Label();
            this.lblusuario = new System.Windows.Forms.Label();
            this.gbplogin = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gpbpermissoes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnvoltar)).BeginInit();
            this.gbplogin.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.BackColor = System.Drawing.Color.Transparent;
            this.lblnome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblnome.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnome.Location = new System.Drawing.Point(8, 24);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(70, 24);
            this.lblnome.TabIndex = 0;
            this.lblnome.Text = "Nome";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(80, 24);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(193, 20);
            this.txtnome.TabIndex = 1;
            // 
            // lbltelefone
            // 
            this.lbltelefone.AutoSize = true;
            this.lbltelefone.BackColor = System.Drawing.Color.Transparent;
            this.lbltelefone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltelefone.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltelefone.Location = new System.Drawing.Point(387, 21);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(93, 24);
            this.lbltelefone.TabIndex = 2;
            this.lbltelefone.Text = "Telefone";
            // 
            // lblcelular
            // 
            this.lblcelular.AutoSize = true;
            this.lblcelular.BackColor = System.Drawing.Color.Transparent;
            this.lblcelular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcelular.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcelular.Location = new System.Drawing.Point(398, 51);
            this.lblcelular.Name = "lblcelular";
            this.lblcelular.Size = new System.Drawing.Size(82, 24);
            this.lblcelular.TabIndex = 3;
            this.lblcelular.Text = "Celular";
            this.lblcelular.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblemail
            // 
            this.lblemail.AutoSize = true;
            this.lblemail.BackColor = System.Drawing.Color.Transparent;
            this.lblemail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblemail.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemail.Location = new System.Drawing.Point(3, 58);
            this.lblemail.Name = "lblemail";
            this.lblemail.Size = new System.Drawing.Size(75, 24);
            this.lblemail.TabIndex = 4;
            this.lblemail.Text = "E-mail";
            // 
            // lblrg
            // 
            this.lblrg.AutoSize = true;
            this.lblrg.BackColor = System.Drawing.Color.Transparent;
            this.lblrg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblrg.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrg.Location = new System.Drawing.Point(439, 86);
            this.lblrg.Name = "lblrg";
            this.lblrg.Size = new System.Drawing.Size(41, 24);
            this.lblrg.TabIndex = 5;
            this.lblrg.Text = "RG";
            // 
            // lblcep
            // 
            this.lblcep.AutoSize = true;
            this.lblcep.BackColor = System.Drawing.Color.Transparent;
            this.lblcep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcep.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcep.Location = new System.Drawing.Point(31, 123);
            this.lblcep.Name = "lblcep";
            this.lblcep.Size = new System.Drawing.Size(52, 24);
            this.lblcep.TabIndex = 6;
            this.lblcep.Text = "CEP";
            // 
            // lblcpf
            // 
            this.lblcpf.AutoSize = true;
            this.lblcpf.BackColor = System.Drawing.Color.Transparent;
            this.lblcpf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcpf.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpf.Location = new System.Drawing.Point(427, 123);
            this.lblcpf.Name = "lblcpf";
            this.lblcpf.Size = new System.Drawing.Size(53, 24);
            this.lblcpf.TabIndex = 7;
            this.lblcpf.Text = "CPF";
            // 
            // lblrua
            // 
            this.lblrua.AutoSize = true;
            this.lblrua.BackColor = System.Drawing.Color.Transparent;
            this.lblrua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblrua.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrua.Location = new System.Drawing.Point(8, 92);
            this.lblrua.Name = "lblrua";
            this.lblrua.Size = new System.Drawing.Size(103, 24);
            this.lblrua.TabIndex = 8;
            this.lblrua.Text = "Endereço";
            // 
            // lblcargo
            // 
            this.lblcargo.AutoSize = true;
            this.lblcargo.BackColor = System.Drawing.Color.Transparent;
            this.lblcargo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcargo.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcargo.Location = new System.Drawing.Point(410, 158);
            this.lblcargo.Name = "lblcargo";
            this.lblcargo.Size = new System.Drawing.Size(70, 24);
            this.lblcargo.TabIndex = 9;
            this.lblcargo.Text = "Cargo";
            // 
            // lblbairro
            // 
            this.lblbairro.AutoSize = true;
            this.lblbairro.BackColor = System.Drawing.Color.Transparent;
            this.lblbairro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblbairro.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbairro.Location = new System.Drawing.Point(5, 157);
            this.lblbairro.Name = "lblbairro";
            this.lblbairro.Size = new System.Drawing.Size(78, 24);
            this.lblbairro.TabIndex = 10;
            this.lblbairro.Text = "Bairro";
            // 
            // cidade
            // 
            this.cidade.AutoSize = true;
            this.cidade.BackColor = System.Drawing.Color.Transparent;
            this.cidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cidade.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cidade.Location = new System.Drawing.Point(5, 196);
            this.cidade.Name = "cidade";
            this.cidade.Size = new System.Drawing.Size(79, 24);
            this.cidade.TabIndex = 11;
            this.cidade.Text = "Cidade";
            // 
            // lblsalario
            // 
            this.lblsalario.AutoSize = true;
            this.lblsalario.BackColor = System.Drawing.Color.Transparent;
            this.lblsalario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblsalario.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsalario.Location = new System.Drawing.Point(398, 194);
            this.lblsalario.Name = "lblsalario";
            this.lblsalario.Size = new System.Drawing.Size(82, 24);
            this.lblsalario.TabIndex = 12;
            this.lblsalario.Text = "Salário";
            // 
            // lblestado
            // 
            this.lblestado.AutoSize = true;
            this.lblestado.BackColor = System.Drawing.Color.Transparent;
            this.lblestado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblestado.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblestado.Location = new System.Drawing.Point(8, 269);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(79, 24);
            this.lblestado.TabIndex = 13;
            this.lblestado.Text = "Estado";
            // 
            // lblcargahoraria
            // 
            this.lblcargahoraria.AutoSize = true;
            this.lblcargahoraria.BackColor = System.Drawing.Color.Transparent;
            this.lblcargahoraria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcargahoraria.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcargahoraria.Location = new System.Drawing.Point(387, 231);
            this.lblcargahoraria.Name = "lblcargahoraria";
            this.lblcargahoraria.Size = new System.Drawing.Size(164, 24);
            this.lblcargahoraria.TabIndex = 14;
            this.lblcargahoraria.Text = "Carga Horária";
            this.lblcargahoraria.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // lblcomplemento
            // 
            this.lblcomplemento.AutoSize = true;
            this.lblcomplemento.BackColor = System.Drawing.Color.Transparent;
            this.lblcomplemento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcomplemento.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcomplemento.Location = new System.Drawing.Point(8, 228);
            this.lblcomplemento.Name = "lblcomplemento";
            this.lblcomplemento.Size = new System.Drawing.Size(145, 24);
            this.lblcomplemento.TabIndex = 15;
            this.lblcomplemento.Text = "Complemento";
            // 
            // lbldatadeadmissao
            // 
            this.lbldatadeadmissao.AutoSize = true;
            this.lbldatadeadmissao.BackColor = System.Drawing.Color.Transparent;
            this.lbldatadeadmissao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbldatadeadmissao.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldatadeadmissao.Location = new System.Drawing.Point(187, 270);
            this.lbldatadeadmissao.Name = "lbldatadeadmissao";
            this.lbldatadeadmissao.Size = new System.Drawing.Size(198, 24);
            this.lbldatadeadmissao.TabIndex = 16;
            this.lbldatadeadmissao.Text = "Data de Admissão";
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(80, 58);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(193, 20);
            this.txtemail.TabIndex = 17;
            // 
            // txtendereco
            // 
            this.txtendereco.Location = new System.Drawing.Point(112, 92);
            this.txtendereco.Name = "txtendereco";
            this.txtendereco.Size = new System.Drawing.Size(250, 20);
            this.txtendereco.TabIndex = 18;
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(84, 157);
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(189, 20);
            this.txtbairro.TabIndex = 19;
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(84, 194);
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(189, 20);
            this.txtcidade.TabIndex = 20;
            // 
            // txtsalario
            // 
            this.txtsalario.Location = new System.Drawing.Point(486, 195);
            this.txtsalario.Name = "txtsalario";
            this.txtsalario.Size = new System.Drawing.Size(134, 20);
            this.txtsalario.TabIndex = 21;
            // 
            // txtcargahoraria
            // 
            this.txtcargahoraria.Location = new System.Drawing.Point(556, 232);
            this.txtcargahoraria.Name = "txtcargahoraria";
            this.txtcargahoraria.Size = new System.Drawing.Size(64, 20);
            this.txtcargahoraria.TabIndex = 22;
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(159, 231);
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(203, 20);
            this.txtcomplemento.TabIndex = 23;
            // 
            // txtcargo
            // 
            this.txtcargo.Location = new System.Drawing.Point(486, 158);
            this.txtcargo.Name = "txtcargo";
            this.txtcargo.Size = new System.Drawing.Size(134, 20);
            this.txtcargo.TabIndex = 24;
            // 
            // dtpdataadmissao
            // 
            this.dtpdataadmissao.Location = new System.Drawing.Point(391, 272);
            this.dtpdataadmissao.Name = "dtpdataadmissao";
            this.dtpdataadmissao.Size = new System.Drawing.Size(229, 20);
            this.dtpdataadmissao.TabIndex = 25;
            // 
            // txttelefone
            // 
            this.txttelefone.Location = new System.Drawing.Point(486, 21);
            this.txttelefone.Mask = "(999) 000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(89, 20);
            this.txttelefone.TabIndex = 26;
            // 
            // txtcelular
            // 
            this.txtcelular.Location = new System.Drawing.Point(486, 51);
            this.txtcelular.Mask = "(999) 000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(89, 20);
            this.txtcelular.TabIndex = 27;
            // 
            // txtrg
            // 
            this.txtrg.Location = new System.Drawing.Point(486, 86);
            this.txtrg.Mask = "99 000-0000 - 0";
            this.txtrg.Name = "txtrg";
            this.txtrg.Size = new System.Drawing.Size(89, 20);
            this.txtrg.TabIndex = 28;
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(486, 124);
            this.txtcpf.Mask = "999 000-0000/00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(89, 20);
            this.txtcpf.TabIndex = 29;
            // 
            // txtcep
            // 
            this.txtcep.Location = new System.Drawing.Point(84, 123);
            this.txtcep.Mask = "00000-000";
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(59, 20);
            this.txtcep.TabIndex = 30;
            // 
            // rdnRH
            // 
            this.rdnRH.AutoSize = true;
            this.rdnRH.BackColor = System.Drawing.Color.Transparent;
            this.rdnRH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdnRH.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnRH.Location = new System.Drawing.Point(0, 32);
            this.rdnRH.Name = "rdnRH";
            this.rdnRH.Size = new System.Drawing.Size(69, 31);
            this.rdnRH.TabIndex = 32;
            this.rdnRH.TabStop = true;
            this.rdnRH.Text = "RH";
            this.rdnRH.UseVisualStyleBackColor = false;
            // 
            // rdnFinanceiro
            // 
            this.rdnFinanceiro.AutoSize = true;
            this.rdnFinanceiro.BackColor = System.Drawing.Color.Transparent;
            this.rdnFinanceiro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdnFinanceiro.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnFinanceiro.Location = new System.Drawing.Point(0, 66);
            this.rdnFinanceiro.Name = "rdnFinanceiro";
            this.rdnFinanceiro.Size = new System.Drawing.Size(151, 31);
            this.rdnFinanceiro.TabIndex = 33;
            this.rdnFinanceiro.TabStop = true;
            this.rdnFinanceiro.Text = "Financeiro";
            this.rdnFinanceiro.UseVisualStyleBackColor = false;
            // 
            // rdnFuncionario
            // 
            this.rdnFuncionario.AutoSize = true;
            this.rdnFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.rdnFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdnFuncionario.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnFuncionario.Location = new System.Drawing.Point(0, 103);
            this.rdnFuncionario.Name = "rdnFuncionario";
            this.rdnFuncionario.Size = new System.Drawing.Size(174, 31);
            this.rdnFuncionario.TabIndex = 34;
            this.rdnFuncionario.TabStop = true;
            this.rdnFuncionario.Text = "Funcionário ";
            this.rdnFuncionario.UseVisualStyleBackColor = false;
            this.rdnFuncionario.CheckedChanged += new System.EventHandler(this.rdnFuncionario_CheckedChanged);
            // 
            // gpbpermissoes
            // 
            this.gpbpermissoes.BackColor = System.Drawing.Color.Transparent;
            this.gpbpermissoes.Controls.Add(this.rdnFuncionario);
            this.gpbpermissoes.Controls.Add(this.rdnRH);
            this.gpbpermissoes.Controls.Add(this.rdnFinanceiro);
            this.gpbpermissoes.Font = new System.Drawing.Font("Lucida Calligraphy", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbpermissoes.Location = new System.Drawing.Point(364, 371);
            this.gpbpermissoes.Name = "gpbpermissoes";
            this.gpbpermissoes.Size = new System.Drawing.Size(196, 157);
            this.gpbpermissoes.TabIndex = 35;
            this.gpbpermissoes.TabStop = false;
            this.gpbpermissoes.Text = "Permissões";
            // 
            // btnvoltar
            // 
            this.btnvoltar.BackColor = System.Drawing.Color.Transparent;
            this.btnvoltar.Image = global::Dona_Cherry.Properties.Resources.VOLTAR;
            this.btnvoltar.Location = new System.Drawing.Point(3, 498);
            this.btnvoltar.Name = "btnvoltar";
            this.btnvoltar.Size = new System.Drawing.Size(53, 46);
            this.btnvoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnvoltar.TabIndex = 36;
            this.btnvoltar.TabStop = false;
            this.btnvoltar.Click += new System.EventHandler(this.btnvoltar_Click);
            // 
            // lstestado
            // 
            this.lstestado.FormattingEnabled = true;
            this.lstestado.Items.AddRange(new object[] {
            "SP",
            "RJ",
            "SC",
            "AM",
            "MG",
            "AC",
            "AL",
            "AP",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RN",
            "RS",
            "RO",
            "RR",
            "SE",
            "TO"});
            this.lstestado.Location = new System.Drawing.Point(84, 263);
            this.lstestado.Name = "lstestado";
            this.lstestado.Size = new System.Drawing.Size(59, 30);
            this.lstestado.TabIndex = 37;
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(96, 76);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(113, 25);
            this.txtsenha.TabIndex = 38;
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(96, 20);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(113, 25);
            this.txtusuario.TabIndex = 39;
            // 
            // lblsenha
            // 
            this.lblsenha.AutoSize = true;
            this.lblsenha.BackColor = System.Drawing.Color.Transparent;
            this.lblsenha.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F);
            this.lblsenha.Location = new System.Drawing.Point(6, 74);
            this.lblsenha.Name = "lblsenha";
            this.lblsenha.Size = new System.Drawing.Size(76, 24);
            this.lblsenha.TabIndex = 40;
            this.lblsenha.Text = "Senha:";
            // 
            // lblusuario
            // 
            this.lblusuario.AutoSize = true;
            this.lblusuario.BackColor = System.Drawing.Color.Transparent;
            this.lblusuario.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F);
            this.lblusuario.Location = new System.Drawing.Point(3, 21);
            this.lblusuario.Name = "lblusuario";
            this.lblusuario.Size = new System.Drawing.Size(75, 24);
            this.lblusuario.TabIndex = 41;
            this.lblusuario.Text = "Nome:";
            // 
            // gbplogin
            // 
            this.gbplogin.BackColor = System.Drawing.Color.Transparent;
            this.gbplogin.Controls.Add(this.txtsenha);
            this.gbplogin.Controls.Add(this.lblsenha);
            this.gbplogin.Controls.Add(this.lblusuario);
            this.gbplogin.Controls.Add(this.txtusuario);
            this.gbplogin.Font = new System.Drawing.Font("Lucida Calligraphy", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbplogin.Location = new System.Drawing.Point(110, 371);
            this.gbplogin.Name = "gbplogin";
            this.gbplogin.Size = new System.Drawing.Size(227, 157);
            this.gbplogin.TabIndex = 42;
            this.gbplogin.TabStop = false;
            this.gbplogin.Text = "Login";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblnome);
            this.groupBox1.Controls.Add(this.lstestado);
            this.groupBox1.Controls.Add(this.lblemail);
            this.groupBox1.Controls.Add(this.lbldatadeadmissao);
            this.groupBox1.Controls.Add(this.dtpdataadmissao);
            this.groupBox1.Controls.Add(this.txtcpf);
            this.groupBox1.Controls.Add(this.txtcargahoraria);
            this.groupBox1.Controls.Add(this.txtcargo);
            this.groupBox1.Controls.Add(this.txtsalario);
            this.groupBox1.Controls.Add(this.lblcargahoraria);
            this.groupBox1.Controls.Add(this.lblcep);
            this.groupBox1.Controls.Add(this.txtrg);
            this.groupBox1.Controls.Add(this.txtcep);
            this.groupBox1.Controls.Add(this.lblsalario);
            this.groupBox1.Controls.Add(this.txtcelular);
            this.groupBox1.Controls.Add(this.lblrua);
            this.groupBox1.Controls.Add(this.txttelefone);
            this.groupBox1.Controls.Add(this.lblcargo);
            this.groupBox1.Controls.Add(this.lblbairro);
            this.groupBox1.Controls.Add(this.cidade);
            this.groupBox1.Controls.Add(this.lblcomplemento);
            this.groupBox1.Controls.Add(this.lblcpf);
            this.groupBox1.Controls.Add(this.txtnome);
            this.groupBox1.Controls.Add(this.lblrg);
            this.groupBox1.Controls.Add(this.txtemail);
            this.groupBox1.Controls.Add(this.txtcomplemento);
            this.groupBox1.Controls.Add(this.txtendereco);
            this.groupBox1.Controls.Add(this.lblcelular);
            this.groupBox1.Controls.Add(this.txtbairro);
            this.groupBox1.Controls.Add(this.txtcidade);
            this.groupBox1.Controls.Add(this.lblestado);
            this.groupBox1.Controls.Add(this.lbltelefone);
            this.groupBox1.Location = new System.Drawing.Point(110, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(657, 327);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(616, 431);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 37);
            this.button1.TabIndex = 44;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Dona_Cherry.Properties.Resources.Flourish_Monogram_10;
            this.pictureBox1.Location = new System.Drawing.Point(566, 391);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(201, 124);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Cadastrar_Funcionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Dona_Cherry.Properties.Resources.babypink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(789, 540);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnvoltar);
            this.Controls.Add(this.gpbpermissoes);
            this.Controls.Add(this.gbplogin);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Cadastrar_Funcionario";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar_Funcionario";
            this.gpbpermissoes.ResumeLayout(false);
            this.gpbpermissoes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnvoltar)).EndInit();
            this.gbplogin.ResumeLayout(false);
            this.gbplogin.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label lbltelefone;
        private System.Windows.Forms.Label lblcelular;
        private System.Windows.Forms.Label lblemail;
        private System.Windows.Forms.Label lblrg;
        private System.Windows.Forms.Label lblcep;
        private System.Windows.Forms.Label lblcpf;
        private System.Windows.Forms.Label lblrua;
        private System.Windows.Forms.Label lblcargo;
        private System.Windows.Forms.Label lblbairro;
        private System.Windows.Forms.Label cidade;
        private System.Windows.Forms.Label lblsalario;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.Label lblcargahoraria;
        private System.Windows.Forms.Label lblcomplemento;
        private System.Windows.Forms.Label lbldatadeadmissao;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txtendereco;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.TextBox txtsalario;
        private System.Windows.Forms.TextBox txtcargahoraria;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.TextBox txtcargo;
        private System.Windows.Forms.DateTimePicker dtpdataadmissao;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.MaskedTextBox txtrg;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.MaskedTextBox txtcep;
        private System.Windows.Forms.RadioButton rdnRH;
        private System.Windows.Forms.RadioButton rdnFinanceiro;
        private System.Windows.Forms.RadioButton rdnFuncionario;
        private System.Windows.Forms.GroupBox gpbpermissoes;
        private System.Windows.Forms.PictureBox btnvoltar;
        private System.Windows.Forms.ListBox lstestado;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.Label lblsenha;
        private System.Windows.Forms.Label lblusuario;
        private System.Windows.Forms.GroupBox gbplogin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}