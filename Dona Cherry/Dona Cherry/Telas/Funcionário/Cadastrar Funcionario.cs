﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.Funcionário
{
    public partial class Cadastrar_Funcionario : Form
    {
        public Cadastrar_Funcionario()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void rdnFuncionario_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
         try
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();

                FuncionarioDTO dto = new FuncionarioDTO();
                dto.nome = txtnome.Text;
                dto.telefonefixo = txttelefone.Text;
                dto.CPF = txtcpf.Text;
                dto.cep = txtcep.Text;
                dto.celular = txtcelular.Text;
                dto.dataadmissao = dtpdataadmissao.Value;
                dto.salario = txtsalario.Text;
                dto.cargahoraria = txtcargahoraria.Text;
                dto.cargo = txtcargo.Text;
                dto.RG = txtrg.Text;
                dto.cidade = txtcidade.Text;
                dto.bairro = txtbairro.Text;
                dto.estado = lstestado.Text;
                dto.email = txtemail.Text;
                dto.complemento = txtcomplemento.Text;
                dto.endereco = txtendereco.Text;
                dto.usuario = txtusuario.Text;
                dto.senha = txtsenha.Text;
                dto.permissaofinanceiro = rdnFinanceiro.Checked;
                dto.permissaofuncionario = rdnFuncionario.Checked;
                dto.permissaorh = rdnRH.Checked;
                                
                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("Funcionário salvo com sucesso.", "Dona Cherry",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                Login_Funcionario funlog = new Login_Funcionario();
                funlog.Show();
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dona Cherry",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                    
            

        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry voltar = new FrmDonaCherry();
            voltar.Show();
            this.Hide();
        }

        private void btnalterar_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();

                FuncionarioDTO dto = new FuncionarioDTO();
                dto.nome = txtnome.Text;
                dto.telefonefixo = txttelefone.Text;
                dto.CPF = txtcpf.Text;
                dto.cep = txtcep.Text;
                dto.celular = txtcelular.Text;
                dto.dataadmissao = dtpdataadmissao.Value;
                dto.salario = txtsalario.Text;
                dto.cargahoraria = txtcargahoraria.Text;
                dto.cargo = txtcargo.Text;
                dto.RG = txtrg.Text;
                dto.cidade = txtcidade.Text;
                dto.bairro = txtbairro.Text;
                dto.estado = lstestado.Text;
                dto.email = txtemail.Text;
                dto.complemento = txtcomplemento.Text;
                dto.endereco = txtendereco.Text;
                dto.usuario = txtusuario.Text;
                dto.senha = txtsenha.Text;
                dto.permissaofinanceiro = rdnFinanceiro.Checked;
                dto.permissaofuncionario = rdnFuncionario.Checked;
                dto.permissaorh = rdnRH.Checked;

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("Funcionário salvo com sucesso.", "Dona Cherry",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                Login_Funcionario funlog = new Login_Funcionario();
                funlog.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
