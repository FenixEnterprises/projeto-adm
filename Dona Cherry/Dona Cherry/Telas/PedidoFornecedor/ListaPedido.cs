﻿using Dona_Cherry.Telas.Fornecedor;
using Dona_Cherry.Telas.Funcionário;
using Dona_Cherry.Telas.ListaPedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.PedidoFornecedor
{
    public partial class ListaPedido : Form
    {
        public ListaPedido()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness bus = new FornecedorBusiness();
            List<FornecedorDTO> lista = bus.Listar();

            cbofornecedor.ValueMember = nameof(FornecedorDTO.id_fornecedor);
            cbofornecedor.DisplayMember = nameof(FornecedorDTO.nm_fantasia);
            cbofornecedor.DataSource = lista;

            FuncionarioBusiness bb = new FuncionarioBusiness();
            List<FuncionarioDTO> kk = bb.ListarPerfilFuncionario();

            cbofornecedor.ValueMember = nameof(FuncionarioDTO.id_funcionario);
            cbofornecedor.DisplayMember = nameof(FuncionarioDTO.nome);
            cbofornecedor.DataSource = lista;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void PedidoFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            
            
        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry tela = new FrmDonaCherry();
            tela.Show();
            this.Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = cbofornecedor.SelectedItem as FornecedorDTO;
                FuncionarioDTO funcionario = cbofornecedor.SelectedItem as FuncionarioDTO;



                ListaPedidoDTO dto = new ListaPedidoDTO();
                dto.nm_produto = txtnmproduto.Text;
                dto.qtd_qtdproduto = Convert.ToInt32(txtquantidade.Text);
                dto.dt_datapedido = dtpdatapedido.Value;
                dto.dt_dataprevista = dtpdataprevista.Value;
                dto.funcionario_id_funcioanrio = funcionario.id_funcionario;
                dto.forneceodr_id_fornecedor = fornecedor.id_fornecedor;


                ListaPedidoBusiness business = new ListaPedidoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Pedido realizado com sucesso.", "Dona Cherry",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
                FrmDonaCherry novo = new FrmDonaCherry();
                novo.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Cadastrar_Funcionario nn = new Cadastrar_Funcionario();
            nn.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cadastro_Fornecedor kk = new Cadastro_Fornecedor();
            kk.Show();
        }
    }
}
