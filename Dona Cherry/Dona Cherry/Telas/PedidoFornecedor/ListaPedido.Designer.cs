﻿namespace Dona_Cherry.Telas.PedidoFornecedor
{
    partial class ListaPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFornecedor = new System.Windows.Forms.Label();
            this.lblQuantidade = new System.Windows.Forms.Label();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.lblDatapedido = new System.Windows.Forms.Label();
            this.lblFormapagamento = new System.Windows.Forms.Label();
            this.txtquantidade = new System.Windows.Forms.TextBox();
            this.txtnmproduto = new System.Windows.Forms.TextBox();
            this.dtpdatapedido = new System.Windows.Forms.DateTimePicker();
            this.dtpdataprevista = new System.Windows.Forms.DateTimePicker();
            this.lblDataentrega = new System.Windows.Forms.Label();
            this.btnvoltar = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cbofuncionario = new System.Windows.Forms.ComboBox();
            this.cbofornecedor = new System.Windows.Forms.ComboBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.btnvoltar)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFornecedor
            // 
            this.lblFornecedor.AutoSize = true;
            this.lblFornecedor.BackColor = System.Drawing.Color.Transparent;
            this.lblFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFornecedor.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFornecedor.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblFornecedor.Location = new System.Drawing.Point(60, 38);
            this.lblFornecedor.Name = "lblFornecedor";
            this.lblFornecedor.Size = new System.Drawing.Size(225, 27);
            this.lblFornecedor.TabIndex = 16;
            this.lblFornecedor.Text = "Nome do Produto";
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.AutoSize = true;
            this.lblQuantidade.BackColor = System.Drawing.Color.Transparent;
            this.lblQuantidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblQuantidade.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblQuantidade.Location = new System.Drawing.Point(132, 87);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(153, 27);
            this.lblQuantidade.TabIndex = 17;
            this.lblQuantidade.Text = "Quantidade";
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.AutoSize = true;
            this.lblFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.lblFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFuncionario.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblFuncionario.Location = new System.Drawing.Point(5, 233);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(276, 27);
            this.lblFuncionario.TabIndex = 18;
            this.lblFuncionario.Text = "Nome do Funcionário";
            this.lblFuncionario.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblDatapedido
            // 
            this.lblDatapedido.AutoSize = true;
            this.lblDatapedido.BackColor = System.Drawing.Color.Transparent;
            this.lblDatapedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDatapedido.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatapedido.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblDatapedido.Location = new System.Drawing.Point(87, 135);
            this.lblDatapedido.Name = "lblDatapedido";
            this.lblDatapedido.Size = new System.Drawing.Size(198, 27);
            this.lblDatapedido.TabIndex = 19;
            this.lblDatapedido.Text = "Data de Pedido";
            // 
            // lblFormapagamento
            // 
            this.lblFormapagamento.AutoSize = true;
            this.lblFormapagamento.BackColor = System.Drawing.Color.Transparent;
            this.lblFormapagamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFormapagamento.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormapagamento.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblFormapagamento.Location = new System.Drawing.Point(18, 290);
            this.lblFormapagamento.Name = "lblFormapagamento";
            this.lblFormapagamento.Size = new System.Drawing.Size(263, 27);
            this.lblFormapagamento.TabIndex = 22;
            this.lblFormapagamento.Text = "Nome do Fornecedor";
            // 
            // txtquantidade
            // 
            this.txtquantidade.Location = new System.Drawing.Point(291, 93);
            this.txtquantidade.Name = "txtquantidade";
            this.txtquantidade.Size = new System.Drawing.Size(254, 20);
            this.txtquantidade.TabIndex = 23;
            // 
            // txtnmproduto
            // 
            this.txtnmproduto.Location = new System.Drawing.Point(291, 44);
            this.txtnmproduto.Name = "txtnmproduto";
            this.txtnmproduto.Size = new System.Drawing.Size(254, 20);
            this.txtnmproduto.TabIndex = 25;
            this.txtnmproduto.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // dtpdatapedido
            // 
            this.dtpdatapedido.Location = new System.Drawing.Point(291, 138);
            this.dtpdatapedido.Name = "dtpdatapedido";
            this.dtpdatapedido.Size = new System.Drawing.Size(254, 20);
            this.dtpdatapedido.TabIndex = 26;
            // 
            // dtpdataprevista
            // 
            this.dtpdataprevista.Location = new System.Drawing.Point(291, 187);
            this.dtpdataprevista.Name = "dtpdataprevista";
            this.dtpdataprevista.Size = new System.Drawing.Size(254, 20);
            this.dtpdataprevista.TabIndex = 28;
            // 
            // lblDataentrega
            // 
            this.lblDataentrega.AutoSize = true;
            this.lblDataentrega.BackColor = System.Drawing.Color.Transparent;
            this.lblDataentrega.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDataentrega.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataentrega.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblDataentrega.Location = new System.Drawing.Point(100, 187);
            this.lblDataentrega.Name = "lblDataentrega";
            this.lblDataentrega.Size = new System.Drawing.Size(185, 27);
            this.lblDataentrega.TabIndex = 20;
            this.lblDataentrega.Text = "Data Prevista";
            // 
            // btnvoltar
            // 
            this.btnvoltar.BackColor = System.Drawing.Color.Transparent;
            this.btnvoltar.Image = global::Dona_Cherry.Properties.Resources.VOLTAR;
            this.btnvoltar.Location = new System.Drawing.Point(549, 386);
            this.btnvoltar.Name = "btnvoltar";
            this.btnvoltar.Size = new System.Drawing.Size(45, 38);
            this.btnvoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnvoltar.TabIndex = 38;
            this.btnvoltar.TabStop = false;
            this.btnvoltar.Click += new System.EventHandler(this.btnvoltar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.cbofuncionario);
            this.groupBox1.Controls.Add(this.cbofornecedor);
            this.groupBox1.Controls.Add(this.btnSalvar);
            this.groupBox1.Controls.Add(this.btnvoltar);
            this.groupBox1.Controls.Add(this.lblFornecedor);
            this.groupBox1.Controls.Add(this.txtnmproduto);
            this.groupBox1.Controls.Add(this.lblFormapagamento);
            this.groupBox1.Controls.Add(this.lblQuantidade);
            this.groupBox1.Controls.Add(this.dtpdataprevista);
            this.groupBox1.Controls.Add(this.txtquantidade);
            this.groupBox1.Controls.Add(this.lblFuncionario);
            this.groupBox1.Controls.Add(this.dtpdatapedido);
            this.groupBox1.Controls.Add(this.lblDatapedido);
            this.groupBox1.Controls.Add(this.lblDataentrega);
            this.groupBox1.Location = new System.Drawing.Point(83, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(600, 425);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(549, 228);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(42, 32);
            this.button2.TabIndex = 41;
            this.button2.Text = "+";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(549, 285);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(42, 32);
            this.button1.TabIndex = 40;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbofuncionario
            // 
            this.cbofuncionario.FormattingEnabled = true;
            this.cbofuncionario.Location = new System.Drawing.Point(291, 233);
            this.cbofuncionario.Name = "cbofuncionario";
            this.cbofuncionario.Size = new System.Drawing.Size(254, 21);
            this.cbofuncionario.TabIndex = 40;
            // 
            // cbofornecedor
            // 
            this.cbofornecedor.FormattingEnabled = true;
            this.cbofornecedor.Location = new System.Drawing.Point(291, 290);
            this.cbofornecedor.Name = "cbofornecedor";
            this.cbofornecedor.Size = new System.Drawing.Size(254, 21);
            this.cbofornecedor.TabIndex = 39;
            // 
            // btnSalvar
            // 
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Font = new System.Drawing.Font("Lucida Calligraphy", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Location = new System.Drawing.Point(251, 354);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(105, 32);
            this.btnSalvar.TabIndex = 33;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // ListaPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Dona_Cherry.Properties.Resources.babypink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(704, 456);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ListaPedido";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PedidoFornecedor";
            this.Load += new System.EventHandler(this.PedidoFornecedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnvoltar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblFornecedor;
        private System.Windows.Forms.Label lblQuantidade;
        private System.Windows.Forms.Label lblFuncionario;
        private System.Windows.Forms.Label lblDatapedido;
        private System.Windows.Forms.Label lblFormapagamento;
        private System.Windows.Forms.TextBox txtquantidade;
        private System.Windows.Forms.TextBox txtnmproduto;
        private System.Windows.Forms.DateTimePicker dtpdatapedido;
        private System.Windows.Forms.DateTimePicker dtpdataprevista;
        private System.Windows.Forms.Label lblDataentrega;
        private System.Windows.Forms.PictureBox btnvoltar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.ComboBox cbofuncionario;
        private System.Windows.Forms.ComboBox cbofornecedor;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}