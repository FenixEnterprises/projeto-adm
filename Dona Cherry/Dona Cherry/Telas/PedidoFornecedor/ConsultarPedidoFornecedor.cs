﻿
using Dona_Cherry.Telas.ListaPedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.PedidoFornecedor
{
    public partial class ConsultarPedidoFornecedor : Form
    {
        public ConsultarPedidoFornecedor()
        {
            InitializeComponent();
        }

        private void ConsultarPedidoFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ListaPedidoBusiness bus = new ListaPedidoBusiness();
                List<ListaPedidoDTO> lista = bus.Listar();

                dgvconsultarpedforn.AutoGenerateColumns = false;
                dgvconsultarpedforn.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvPedidoConsultarForn_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnsalvar_Click(object sender, EventArgs e)
        {

        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            FrmDonaCherry tela = new FrmDonaCherry();
            tela.Show();
            this.Hide();

        }

        private void btnremover_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvconsultarpedforn.CurrentRow != null)
                {
                    ListaPedidoDTO pedido = dgvconsultarpedforn.CurrentRow.DataBoundItem as ListaPedidoDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse pedido?", "Dona Cherry",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        ListaPedidoBusiness business = new ListaPedidoBusiness();
                        business.Remover(pedido.id_listapedido);

                        List<ListaPedidoDTO> a = business.Listar();
                        dgvconsultarpedforn.AutoGenerateColumns = false;
                        dgvconsultarpedforn.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um Pedido");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }


}

        