﻿using Dona_Cherry.DonaCherry.Compras;
using Dona_Cherry.Telas.Fornecedor;
using Dona_Cherry.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.PedidoFornecedor
{
    public partial class RegistrarCompras : Form
    {
        public RegistrarCompras()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness bus = new FornecedorBusiness();
            List<FornecedorDTO> lista = bus.Listar();

            cbofornecedor.ValueMember = nameof(FornecedorDTO.id_fornecedor);
            cbofornecedor.DisplayMember = nameof(FornecedorDTO.nm_fantasia);
            cbofornecedor.DataSource = lista;


            ProdutoBusiness bb = new ProdutoBusiness();
            List<ProdutoDTO> kk = bb.Listar();

            cbofornecedor.ValueMember = nameof(ProdutoDTO.id);
            cbofornecedor.DisplayMember = nameof(ProdutoDTO.produto);
            cbofornecedor.DataSource = lista;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cadastro_Fornecedor nn = new Cadastro_Fornecedor();
            nn.Show();
            this.Hide();
        }

        private void RegistrarCompras_Load(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = cbofornecedor.SelectedItem as FornecedorDTO;
                ProdutoDTO produto = cboproduto.SelectedItem as ProdutoDTO;

                ComprasDTO dto = new ComprasDTO();
                dto.id_produto = produto.id;
                dto.valor = Convert.ToDecimal(txtvalor.Text);
                dto.quantidade = Convert.ToInt32(txtqtd.Text);
                dto.datacompra = dtpdatacompra.Value;
                dto.id_fornecedor = fornecedor.id_fornecedor;


                ComprasBusiness compras = new ComprasBusiness();
                compras.Salvar(dto);

                MessageBox.Show("Compra salva com sucesso.", "Dona Cherry",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                FrmDonaCherry nv = new FrmDonaCherry();
                nv.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
    }

