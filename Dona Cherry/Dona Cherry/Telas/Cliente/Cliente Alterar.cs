﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.Cliente
{
    public partial class Cliente_Alterar : Form
    {
        public Cliente_Alterar()
        {
            InitializeComponent();
        }
        ClienteDTO cliente = new ClienteDTO();
        public void LoadScreen(ClienteDTO dto)
        {
            this.cliente = dto;
        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                
                ClienteDTO dto = new ClienteDTO();
                
                dto.nome = txtnome.Text;
                dto.bairro = txtbairro.Text;
                dto.cep = txtcep.Text;
                dto.complemento = txtcomplemento.Text;
                dto.cpf = txtcpf.Text;
                dto.telefonefixo = msktelefone.Text;
                dto.celular = mkscelular.Text;
                dto.cidade = txtcidade.Text;
                dto.endereco = txtendereco.Text;
                dto.id_cliente = cliente.id_cliente;

                ClienteBusiness buss = new ClienteBusiness();
                buss.Alterar(dto);

                MessageBox.Show("Cliente alterado com suceso!!", "Dona Cherry", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
