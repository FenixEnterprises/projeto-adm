﻿namespace Dona_Cherry.Telas.Cliente
{
    partial class Cliente_Alterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNome = new System.Windows.Forms.Label();
            this.lblcep = new System.Windows.Forms.Label();
            this.lblbairro = new System.Windows.Forms.Label();
            this.lblcomplemento = new System.Windows.Forms.Label();
            this.lblcidade = new System.Windows.Forms.Label();
            this.lblcpf = new System.Windows.Forms.Label();
            this.lblcelular = new System.Windows.Forms.Label();
            this.lbltelefone = new System.Windows.Forms.Label();
            this.lblendereco = new System.Windows.Forms.Label();
            this.msktelefone = new System.Windows.Forms.MaskedTextBox();
            this.mkscelular = new System.Windows.Forms.MaskedTextBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.txtcep = new System.Windows.Forms.MaskedTextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtendereco = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.txtcpf = new System.Windows.Forms.TextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.BackColor = System.Drawing.Color.Transparent;
            this.lblNome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNome.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblNome.Location = new System.Drawing.Point(56, 29);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(83, 27);
            this.lblNome.TabIndex = 2;
            this.lblNome.Text = "Nome";
            // 
            // lblcep
            // 
            this.lblcep.AutoSize = true;
            this.lblcep.BackColor = System.Drawing.Color.Transparent;
            this.lblcep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcep.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcep.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblcep.Location = new System.Drawing.Point(56, 186);
            this.lblcep.Name = "lblcep";
            this.lblcep.Size = new System.Drawing.Size(60, 27);
            this.lblcep.TabIndex = 3;
            this.lblcep.Text = "CEP";
            // 
            // lblbairro
            // 
            this.lblbairro.AutoSize = true;
            this.lblbairro.BackColor = System.Drawing.Color.Transparent;
            this.lblbairro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblbairro.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbairro.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblbairro.Location = new System.Drawing.Point(56, 105);
            this.lblbairro.Name = "lblbairro";
            this.lblbairro.Size = new System.Drawing.Size(93, 27);
            this.lblbairro.TabIndex = 4;
            this.lblbairro.Text = "Bairro";
            // 
            // lblcomplemento
            // 
            this.lblcomplemento.AutoSize = true;
            this.lblcomplemento.BackColor = System.Drawing.Color.Transparent;
            this.lblcomplemento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcomplemento.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcomplemento.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblcomplemento.Location = new System.Drawing.Point(56, 267);
            this.lblcomplemento.Name = "lblcomplemento";
            this.lblcomplemento.Size = new System.Drawing.Size(173, 27);
            this.lblcomplemento.TabIndex = 5;
            this.lblcomplemento.Text = "Complemento";
            // 
            // lblcidade
            // 
            this.lblcidade.AutoSize = true;
            this.lblcidade.BackColor = System.Drawing.Color.Transparent;
            this.lblcidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcidade.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcidade.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblcidade.Location = new System.Drawing.Point(261, 192);
            this.lblcidade.Name = "lblcidade";
            this.lblcidade.Size = new System.Drawing.Size(93, 27);
            this.lblcidade.TabIndex = 6;
            this.lblcidade.Text = "Cidade";
            // 
            // lblcpf
            // 
            this.lblcpf.AutoSize = true;
            this.lblcpf.BackColor = System.Drawing.Color.Transparent;
            this.lblcpf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcpf.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpf.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblcpf.Location = new System.Drawing.Point(502, 22);
            this.lblcpf.Name = "lblcpf";
            this.lblcpf.Size = new System.Drawing.Size(61, 27);
            this.lblcpf.TabIndex = 7;
            this.lblcpf.Text = "CPF";
            // 
            // lblcelular
            // 
            this.lblcelular.AutoSize = true;
            this.lblcelular.BackColor = System.Drawing.Color.Transparent;
            this.lblcelular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcelular.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcelular.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblcelular.Location = new System.Drawing.Point(304, 267);
            this.lblcelular.Name = "lblcelular";
            this.lblcelular.Size = new System.Drawing.Size(99, 27);
            this.lblcelular.TabIndex = 8;
            this.lblcelular.Text = "Celular";
            // 
            // lbltelefone
            // 
            this.lbltelefone.AutoSize = true;
            this.lbltelefone.BackColor = System.Drawing.Color.Transparent;
            this.lbltelefone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltelefone.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltelefone.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbltelefone.Location = new System.Drawing.Point(261, 105);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(111, 27);
            this.lbltelefone.TabIndex = 9;
            this.lbltelefone.Text = "Telefone";
            // 
            // lblendereco
            // 
            this.lblendereco.AutoSize = true;
            this.lblendereco.BackColor = System.Drawing.Color.Transparent;
            this.lblendereco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblendereco.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblendereco.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblendereco.Location = new System.Drawing.Point(251, 29);
            this.lblendereco.Name = "lblendereco";
            this.lblendereco.Size = new System.Drawing.Size(121, 27);
            this.lblendereco.TabIndex = 10;
            this.lblendereco.Text = "Endereço";
            // 
            // msktelefone
            // 
            this.msktelefone.Location = new System.Drawing.Point(396, 105);
            this.msktelefone.Mask = "(99) 000-0000";
            this.msktelefone.Name = "msktelefone";
            this.msktelefone.Size = new System.Drawing.Size(79, 20);
            this.msktelefone.TabIndex = 11;
            // 
            // mkscelular
            // 
            this.mkscelular.Location = new System.Drawing.Point(409, 273);
            this.mkscelular.Mask = "(99) 000-0000";
            this.mkscelular.Name = "mkscelular";
            this.mkscelular.Size = new System.Drawing.Size(79, 20);
            this.mkscelular.TabIndex = 12;
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(145, 105);
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(100, 20);
            this.txtbairro.TabIndex = 14;
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(235, 273);
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(54, 20);
            this.txtcomplemento.TabIndex = 15;
            // 
            // txtcep
            // 
            this.txtcep.Location = new System.Drawing.Point(145, 193);
            this.txtcep.Mask = "0000-000";
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(101, 20);
            this.txtcep.TabIndex = 39;
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(145, 29);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(100, 20);
            this.txtnome.TabIndex = 13;
            // 
            // txtendereco
            // 
            this.txtendereco.Location = new System.Drawing.Point(396, 29);
            this.txtendereco.Name = "txtendereco";
            this.txtendereco.Size = new System.Drawing.Size(100, 20);
            this.txtendereco.TabIndex = 40;
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(396, 192);
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(100, 20);
            this.txtcidade.TabIndex = 41;
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(569, 22);
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(100, 20);
            this.txtcpf.TabIndex = 42;
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.Transparent;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Font = new System.Drawing.Font("Lucida Calligraphy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Location = new System.Drawing.Point(516, 308);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(132, 62);
            this.btnSalvar.TabIndex = 44;
            this.btnSalvar.Text = "Salvar Alterações ";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Dona_Cherry.Properties.Resources.Flourish_Monogram_10;
            this.pictureBox1.Location = new System.Drawing.Point(465, 246);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(230, 186);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Cliente_Alterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.BackgroundImage = global::Dona_Cherry.Properties.Resources.babypink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(681, 420);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.txtcidade);
            this.Controls.Add(this.txtendereco);
            this.Controls.Add(this.txtcep);
            this.Controls.Add(this.txtcomplemento);
            this.Controls.Add(this.txtbairro);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.mkscelular);
            this.Controls.Add(this.msktelefone);
            this.Controls.Add(this.lblendereco);
            this.Controls.Add(this.lbltelefone);
            this.Controls.Add(this.lblcelular);
            this.Controls.Add(this.lblcpf);
            this.Controls.Add(this.lblcidade);
            this.Controls.Add(this.lblcomplemento);
            this.Controls.Add(this.lblbairro);
            this.Controls.Add(this.lblcep);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Cliente_Alterar";
            this.ShowIcon = false;
            this.Text = "Cliente_Alterar";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblcep;
        private System.Windows.Forms.Label lblbairro;
        private System.Windows.Forms.Label lblcomplemento;
        private System.Windows.Forms.Label lblcidade;
        private System.Windows.Forms.Label lblcpf;
        private System.Windows.Forms.Label lblcelular;
        private System.Windows.Forms.Label lbltelefone;
        private System.Windows.Forms.Label lblendereco;
        private System.Windows.Forms.MaskedTextBox msktelefone;
        private System.Windows.Forms.MaskedTextBox mkscelular;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.MaskedTextBox txtcep;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtendereco;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.TextBox txtcpf;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}