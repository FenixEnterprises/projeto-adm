﻿using Dona_Cherry.Telas.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry
{
    public partial class Cadastrar_Cliente : Form
    {
        ClienteDTO ClienteCriado { get; set; }

        public Cadastrar_Cliente()
        {
            InitializeComponent();
        }

        private void lblTelefone_Click(object sender, EventArgs e)
        {

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry voltar = new FrmDonaCherry();
            voltar.Show();
            this.Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            

        }
        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void BtnSalvar_Click(object sender, EventArgs e)
        {

        }

        private void txtbairro_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cadastro = new ClienteDTO();

                ClienteDTO cliente = new ClienteDTO();
                cliente.nome = txtnome.Text;
                cliente.bairro = txtbairro.Text;
                cliente.cep = txtcep.Text;
                cliente.complemento = txtcomplemento.Text;
                cliente.telefonefixo = txttelefone.Text;
                cliente.endereco = txtendereco.Text;
                cliente.cidade = txtcidade.Text;
                cliente.celular = txtcelular.Text;
                cliente.cpf = txtcpf.Text;




                ClienteBusiness business = new ClienteBusiness();
                business.Salvar(cliente);

                MessageBox.Show("Cliente salvo com sucesso.", "Dona Cherry",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnVoltar_Click_1(object sender, EventArgs e)
        {
            FrmDonaCherry jj = new FrmDonaCherry();
            jj.Show();
            this.Hide();
        }
    }
}
