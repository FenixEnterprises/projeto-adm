﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.Cliente
{
    public partial class Consultar_Cliente : Form
    {
        public Consultar_Cliente()
        {
            InitializeComponent();
        }

        private void btnlistar_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteBusiness bus = new ClienteBusiness();
                List<ClienteDTO> lista = bus.Listar();

                dgvconsultarcliente.AutoGenerateColumns = false;
                dgvconsultarcliente.DataSource = lista;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void txtcliente_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            Cliente_Alterar alterar = new Cliente_Alterar();
            alterar.Show();
            this.Hide();

        }



        private void btnremover_Click(object sender, EventArgs e)
        {

            try
            {
                if (dgvconsultarcliente.CurrentRow != null)
                {
                    ClienteDTO cliente = dgvconsultarcliente.CurrentRow.DataBoundItem as ClienteDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse cliente?", "Dona Cherry",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        ClienteBusiness business = new ClienteBusiness();
                        business.Remover(cliente.id_cliente);

                        List<ClienteDTO> a = business.Listar();
                        dgvconsultarcliente.AutoGenerateColumns = false;
                        dgvconsultarcliente.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um Cliente");
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        private void Consultar_Cliente_Load(object sender, EventArgs e)
        {

        }

        private void dgvconsultarcliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnsalvar_Click(object sender, EventArgs e)
        {

            try
            {
                ClienteBusiness bus = new ClienteBusiness();
                List<ClienteDTO> lista = bus.Consultar(txtcliente.Text);

                dgvconsultarcliente.AutoGenerateColumns = false;
                dgvconsultarcliente.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
    }
}
