﻿using Dona_Cherry.DB.Estoque;
using Dona_Cherry.Telas.Fornecedor;
using Dona_Cherry.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry
{
    public partial class Cadastrar_Produto : Form
    {
        public Cadastrar_Produto()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness bus = new FornecedorBusiness();
            List<FornecedorDTO> lista = bus.Listar();

            cbofornecedor.ValueMember = nameof(FornecedorDTO.id_fornecedor);
            cbofornecedor.DisplayMember = nameof(FornecedorDTO.nm_fantasia);
            cbofornecedor.DataSource = lista;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {



        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {



        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry tela = new FrmDonaCherry();
            tela.Show();
            this.Hide();
        }

        private void Cadastrar_Produto_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = cbofornecedor.SelectedItem as FornecedorDTO;

                ProdutoDTO dto = new ProdutoDTO();
                dto.produto = txtproduto.Text.Trim();
                dto.preco = Convert.ToDecimal(txtpreco.Text.Trim());
                dto.peso = Convert.ToDecimal(txtpeso.Text.Trim());
                dto.marca = txtmarca.Text.Trim();
                dto.qtdproduto = Convert.ToInt32(txtqtd.Text.Trim());
                dto.fornecedor_id_fornecedor = fornecedor.id_fornecedor;

                ProdutoBusiness business = new ProdutoBusiness();
                int idproduto = business.Salvar(dto);
                EstoqueBusiness estoque = new EstoqueBusiness();
                EstoqueDTO estoquedto = new EstoqueDTO() { id_produto = idproduto, qtd_totalprodutos = 0 };
                estoque.Salvar(estoquedto);

                MessageBox.Show("Produto salvo com sucesso.", "Dona Cherry",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                FrmDonaCherry nv = new FrmDonaCherry();
                nv.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtpreco_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
