﻿using Dona_Cherry.DB.ProdutoVenda;
using Dona_Cherry.DonaCherry;
using Dona_Cherry.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas.ProdutoVenda
{
    public partial class CadastrarCardápio : Form
    {
        public CadastrarCardápio()
        {
            InitializeComponent();
           
        }

        

        private void button1_Click(object sender, EventArgs e)
        {


            try
            {
                ProdutoVendaDTO dto = new ProdutoVendaDTO();
                dto.valor = Convert.ToDecimal(txtpreco.Text.Trim());
                dto.Imagem = ImagemPlugin.ConverterParaString(imgProduto.Image);
                dto.nome = txtnome.Text;
                dto.descricao = txtdescricao.Text;

                ProdutoVendaBusiness business = new ProdutoVendaBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso.", "Dona Cherry",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            

        }

        private void imgProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgProduto.ImageLocation = dialog.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmDonaCherry nv = new FrmDonaCherry();
            nv.Show();
            this.Hide();
        }

        private void CadastrarCardápio_Load(object sender, EventArgs e)
        {

        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
