﻿namespace Dona_Cherry.Telas
{
    partial class MenuBolos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnomeprato = new System.Windows.Forms.Label();
            this.txtbolonome = new System.Windows.Forms.TextBox();
            this.dgvconsultabolo = new System.Windows.Forms.DataGridView();
            this.btnlistar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnvoltar = new System.Windows.Forms.Button();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnremover = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvconsultabolo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblnomeprato
            // 
            this.lblnomeprato.AutoSize = true;
            this.lblnomeprato.BackColor = System.Drawing.Color.Transparent;
            this.lblnomeprato.Font = new System.Drawing.Font("Lucida Calligraphy", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnomeprato.Location = new System.Drawing.Point(72, 66);
            this.lblnomeprato.Name = "lblnomeprato";
            this.lblnomeprato.Size = new System.Drawing.Size(154, 23);
            this.lblnomeprato.TabIndex = 0;
            this.lblnomeprato.Text = "Nome do Bolo:";
            // 
            // txtbolonome
            // 
            this.txtbolonome.Location = new System.Drawing.Point(75, 96);
            this.txtbolonome.Name = "txtbolonome";
            this.txtbolonome.Size = new System.Drawing.Size(296, 20);
            this.txtbolonome.TabIndex = 1;
            // 
            // dgvconsultabolo
            // 
            this.dgvconsultabolo.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dgvconsultabolo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvconsultabolo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Nome,
            this.Valor,
            this.Descricao});
            this.dgvconsultabolo.Location = new System.Drawing.Point(75, 134);
            this.dgvconsultabolo.Name = "dgvconsultabolo";
            this.dgvconsultabolo.Size = new System.Drawing.Size(649, 364);
            this.dgvconsultabolo.TabIndex = 2;
            // 
            // btnlistar
            // 
            this.btnlistar.BackColor = System.Drawing.Color.Transparent;
            this.btnlistar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnlistar.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnlistar.Location = new System.Drawing.Point(495, 66);
            this.btnlistar.Name = "btnlistar";
            this.btnlistar.Size = new System.Drawing.Size(121, 51);
            this.btnlistar.TabIndex = 3;
            this.btnlistar.Text = "Listar";
            this.btnlistar.UseVisualStyleBackColor = false;
            this.btnlistar.Click += new System.EventHandler(this.btnlistar_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(392, 66);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 51);
            this.button2.TabIndex = 4;
            this.button2.Text = "Consultar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnvoltar
            // 
            this.btnvoltar.BackColor = System.Drawing.Color.Transparent;
            this.btnvoltar.BackgroundImage = global::Dona_Cherry.Properties.Resources.VOLTAR;
            this.btnvoltar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnvoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnvoltar.ForeColor = System.Drawing.Color.Transparent;
            this.btnvoltar.Location = new System.Drawing.Point(740, 473);
            this.btnvoltar.Name = "btnvoltar";
            this.btnvoltar.Size = new System.Drawing.Size(45, 34);
            this.btnvoltar.TabIndex = 8;
            this.btnvoltar.UseVisualStyleBackColor = false;
            this.btnvoltar.Click += new System.EventHandler(this.btnvoltar_Click_1);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "id";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            // 
            // Valor
            // 
            this.Valor.DataPropertyName = "valor";
            this.Valor.HeaderText = "Valor";
            this.Valor.Name = "Valor";
            // 
            // Descricao
            // 
            this.Descricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Descricao.DataPropertyName = "descricao";
            this.Descricao.HeaderText = "Descrição";
            this.Descricao.Name = "Descricao";
            // 
            // btnremover
            // 
            this.btnremover.BackColor = System.Drawing.Color.Transparent;
            this.btnremover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnremover.Font = new System.Drawing.Font("Lucida Calligraphy", 12.75F, System.Drawing.FontStyle.Bold);
            this.btnremover.ForeColor = System.Drawing.Color.Black;
            this.btnremover.Location = new System.Drawing.Point(607, 66);
            this.btnremover.Name = "btnremover";
            this.btnremover.Size = new System.Drawing.Size(55, 51);
            this.btnremover.TabIndex = 9;
            this.btnremover.Text = "X";
            this.btnremover.UseVisualStyleBackColor = false;
            this.btnremover.Click += new System.EventHandler(this.btnremover_Click);
            // 
            // MenuBolos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Dona_Cherry.Properties.Resources.babypink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(782, 507);
            this.Controls.Add(this.btnremover);
            this.Controls.Add(this.btnvoltar);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnlistar);
            this.Controls.Add(this.dgvconsultabolo);
            this.Controls.Add(this.txtbolonome);
            this.Controls.Add(this.lblnomeprato);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MenuBolos";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuBolos";
            ((System.ComponentModel.ISupportInitialize)(this.dgvconsultabolo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblnomeprato;
        private System.Windows.Forms.TextBox txtbolonome;
        private System.Windows.Forms.DataGridView dgvconsultabolo;
        private System.Windows.Forms.Button btnlistar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnvoltar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descricao;
        private System.Windows.Forms.Button btnremover;
    }
}