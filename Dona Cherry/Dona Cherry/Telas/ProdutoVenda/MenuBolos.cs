﻿using Dona_Cherry.DB.ProdutoVenda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dona_Cherry.Telas
{
    public partial class MenuBolos : Form
    {
        public MenuBolos()
        {
            InitializeComponent();
        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            FrmDonaCherry tela = new FrmDonaCherry();
            tela.Show();
            this.Hide();
        }

        private void btnlistar_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoVendaBusiness bus = new ProdutoVendaBusiness();
                List<ProdutoVendaDTO> lista = bus.Listar();

                dgvconsultabolo.AutoGenerateColumns = false;
                dgvconsultabolo.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        private void btnvoltar_Click_1(object sender, EventArgs e)
        {
            FrmDonaCherry fm = new FrmDonaCherry();
            fm.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoVendaBusiness business = new ProdutoVendaBusiness();
                List<ProdutoVendaDTO> lista = business.Consultar(txtbolonome.Text);

                dgvconsultabolo.AutoGenerateColumns = false;
                dgvconsultabolo.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "Dona Cherry",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnremover_Click(object sender, EventArgs e)
        {
            
                if (dgvconsultabolo.CurrentRow != null)
                {
                    ProdutoVendaDTO fornecedor = dgvconsultabolo.CurrentRow.DataBoundItem as ProdutoVendaDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse item do cardápio?", "Dona Cherry",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        ProdutoVendaBusiness business = new ProdutoVendaBusiness();
                        business.Remover(fornecedor.id);

                        List<ProdutoVendaDTO> a = business.Consultar(txtbolonome.Text);
                        dgvconsultabolo.AutoGenerateColumns = false;
                        dgvconsultabolo.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um item.");
                    }

                }
            }
    }
}
