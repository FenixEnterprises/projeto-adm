﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.ListaPedido
{
    class ListaPedidoDTO
    {
        public int id_listapedido { get; set; }
        public string nm_produto { get; set; }
        public int qtd_qtdproduto { get; set; }
        public DateTime dt_datapedido { get; set; }
        public DateTime dt_dataprevista { get; set; }
        public int funcionario_id_funcioanrio { get; set; }
        public int forneceodr_id_fornecedor { get; set; }


    }
}
