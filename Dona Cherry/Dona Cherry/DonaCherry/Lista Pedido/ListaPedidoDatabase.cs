﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.ListaPedido
{
    class ListaPedidoDatabase
    {


        public int Salvar(ListaPedidoDTO dto)
        {
            string script = @"INSERT INTO tb_listapedido(nm_produto, qtd_qtdproduto, dt_datapedido, dt_dataprevista, nm_funcionario, nm_fornecedor) 
                                                 VALUES (@nm_produto, @qtd_qtdproduto, @dt_datapedido, @dt_dataprevista,  @nm_funcionario, @nm_fornecedor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.nm_produto));
            parms.Add(new MySqlParameter("qtd_qtdproduto", dto.qtd_qtdproduto));
            parms.Add(new MySqlParameter("dt_dataprevista", dto.dt_dataprevista));
            parms.Add(new MySqlParameter("dt_datapedido", dto.dt_datapedido));
            parms.Add(new MySqlParameter("funcionario_id_funcioanrio", dto.funcionario_id_funcioanrio));
            parms.Add(new MySqlParameter("forneceodr_id_fornecedor", dto.forneceodr_id_fornecedor));
            


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }




        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_listapedido WHERE id_listapedido = @id_listapedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_listapedido", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }




        public List<ListaPedidoDTO> ConsultarPorPedido(int idPedido)
        {
            string script = @"SELECT * FROM tb_listapedido WHERE id_listapedido = @id_listapedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_listapedido", idPedido));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ListaPedidoDTO> lista = new List<ListaPedidoDTO>();
            while (reader.Read())
            {
                ListaPedidoDTO dto = new ListaPedidoDTO();
                dto.id_listapedido = reader.GetInt32("id_listapedido");
                dto.nm_produto = reader.GetString("nm_produto");
                dto.dt_datapedido = reader.GetDateTime("dt_datapedido");
                dto.qtd_qtdproduto = reader.GetInt32("qtd_qtdproduto");
                dto.dt_datapedido = reader.GetDateTime("qtd_datapedido");
                dto.forneceodr_id_fornecedor = reader.GetInt32("fornecedor_id_fornecedor");
                dto.funcionario_id_funcioanrio = reader.GetInt32("funcionario_id_funcionario");
               

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<ListaPedidoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_listapedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ListaPedidoDTO> lista = new List<ListaPedidoDTO>();
            while (reader.Read())
            {
                ListaPedidoDTO dto = new ListaPedidoDTO();
                dto.id_listapedido = reader.GetInt32("id_listapedido");
                dto.nm_produto = reader.GetString("nm_produto");
                dto.qtd_qtdproduto = reader.GetInt32("qtd_qtdproduto");
                dto.dt_datapedido = reader.GetDateTime("dt_datapedido");
                dto.funcionario_id_funcioanrio = reader.GetInt32("nm_funcionario");
                dto.forneceodr_id_fornecedor = reader.GetInt32("forneceodr_id_fornecedor");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }

}
