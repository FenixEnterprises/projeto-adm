﻿using Dona_Cherry.Telas.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.ListaPedido
{
    class ListaPedidoBusiness
    {
        ListaPedidoDatabase db = new ListaPedidoDatabase();

        public int Salvar(ListaPedidoDTO dto)
        {
            if (dto.nm_produto == string.Empty)
            {
                throw new ArgumentException("O nome do produto é obrigatório.");
            }
            if (dto.qtd_qtdproduto <= 0)
            {
                throw new ArgumentException("A quantidade do produto é obrigatório.");
            }
            if (dto.dt_datapedido == DateTime.Now)
            {
                throw new ArgumentException("A data do pedido é obrigatória.");
            }

            if (dto.dt_dataprevista == DateTime.Now)
            {
                throw new ArgumentException("A data prevista é obrigatória.");
            }

            if (dto.funcionario_id_funcioanrio == 0)
            {
                throw new ArgumentException("O nome do funcionário é obrigatório.");
            }

            if(dto.forneceodr_id_fornecedor == 0)
            {
                throw new ArgumentException("O nome do fornecedor é obrigátório.");
            }

            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            ListaPedidoDatabase db = new ListaPedidoDatabase();
            db.Remover(id);
        }

        public List<ListaPedidoDTO> ConsultarPorPedido(int idPedido)
        {
            ListaPedidoDatabase db = new ListaPedidoDatabase();
            return db.ConsultarPorPedido(idPedido);
        }

        public List<ListaPedidoDTO> Listar()
        {
            ListaPedidoDatabase db = new ListaPedidoDatabase();
            return db.Listar();

        }
    }

}

