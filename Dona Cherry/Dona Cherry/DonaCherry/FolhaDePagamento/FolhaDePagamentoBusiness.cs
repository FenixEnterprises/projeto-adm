﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Dona_Cherry.DonaCherry.FolhaDePagamento
{
    class FolhaDePagamentoBusiness
    {
        FolhaDePagamentoDatabase db = new FolhaDePagamentoDatabase();



        public int Salvar(FolhaDePagamentoDTO folha)
        {

            return db.Salvar(folha);

        }



        
        public void Alterar(FolhaDePagamentoDTO folha)
        {
            db.Alterar(folha);
        }




        public List<FolhaDePagamentoDTO> Consultar()
        {
            return db.Consultar();
        }




        public void Remover(int id)
        {
            db.Remover(id);
        }
    }
}
