﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Dona_Cherry.DonaCherry.FolhaDePagamento
{
    class CalculoFolha
    {

        public decimal CalcularVT(decimal salariobruto, decimal valetransporte)
        {
            decimal porcentagemVT = 0.6m;
            valetransporte = salariobruto * porcentagemVT;
            return valetransporte;
        }




        public decimal CalcularINSS(decimal SalarioBruto, decimal TotalSalarioBruto)
        {
            decimal PorcentagemINSS = 0;
            if (SalarioBruto <= 1693.72m)
            {
                PorcentagemINSS = 8;
            }
            if (SalarioBruto >= 1693.73m && SalarioBruto <= 2822.90m)
            {
                PorcentagemINSS = 9;
            }
            if (SalarioBruto >= 2822.91m && SalarioBruto <= 5645.80m)
            {
                PorcentagemINSS = 11;
            }

            decimal INSS = TotalSalarioBruto * (PorcentagemINSS / 100);

            return INSS;
        }




        public decimal CalcularVR(decimal salariobruto, decimal valerefeicao)
        {
            decimal porcentagemVR = 0.10m;
            valerefeicao = salariobruto * porcentagemVR;
            return valerefeicao;
        }





        public decimal CalcularIR(decimal TotalSalarioBruto, decimal ValorINSS)
        {
            decimal PorcentagemIR = 0;
            decimal ValordeDesconto = 0;

            TotalSalarioBruto = TotalSalarioBruto - ValorINSS;

            if (TotalSalarioBruto >= 1903.99m && TotalSalarioBruto <= 2826.65m)
            {
                PorcentagemIR = 7.5m;
                ValordeDesconto = 142.80m;
            }
            if (TotalSalarioBruto >= 2826.66m && TotalSalarioBruto <= 3751.05m)
            {
                PorcentagemIR = 15;
                ValordeDesconto = 354.80m;
            }
            if (TotalSalarioBruto >= 3751.06m && TotalSalarioBruto <= 4664.68m)
            {
                PorcentagemIR = 22.5m;
                ValordeDesconto = 636.13m;
            }
            if (TotalSalarioBruto > 4664.69m)
            {
                PorcentagemIR = 27.5m;
                ValordeDesconto = 869.36m;
            }

            decimal valordescontado = TotalSalarioBruto * (PorcentagemIR / 100);
            decimal IR = valordescontado - ValordeDesconto;

            return IR;
        }

        public decimal CalcularSalarioLiquido(decimal SalarioBruto, decimal ValorINSS, decimal ValeRefeicao, decimal ValeTransporte, decimal TotalSalarioBruto)

        {
                decimal IR = CalcularIR(SalarioBruto, ValorINSS);
                decimal VT = CalcularVT(SalarioBruto, ValeTransporte);
                decimal VR = CalcularVR(SalarioBruto, ValeRefeicao);
                decimal INSS = CalcularINSS(SalarioBruto, TotalSalarioBruto);

                decimal total = TotalSalarioBruto + ValorINSS + ValeRefeicao + ValeTransporte;
                return total;
            

        }
    }
}