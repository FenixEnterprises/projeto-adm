﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Dona_Cherry.DonaCherry.FolhaDePagamento
{
    class FolhaDePagamentoDatabase
    {
        public int Salvar(FolhaDePagamentoDTO folha)
        {
            string script =
           @"INSERT INTO tb_folhapagamento
                (
                id_folhadepagamento,
                id_funcionario,   
                vl_transporte,
                vl_inss,     
                vl_salariobruto,
                vl_salarioliquido,
                vl_vr,
                vl_ir    
                )
                VALUES
                (
                @id_folhadepagamento,
                @id_funcionario,
                @vl_transporte,
                @vl_inss,     
                @vl_salariobruto,
                @vl_salarioliquido,
                @vl_vr,
                @vl_ir
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folhadepagamento", folha.id_folhadepagamento));
            parms.Add(new MySqlParameter("id_funcionario", folha.id_funcionario));
            parms.Add(new MySqlParameter("vl_transporte", folha.transporte));
            parms.Add(new MySqlParameter("vl_inss", folha.inss));
            parms.Add(new MySqlParameter("vl_salariobruto", folha.salariobruto));
            parms.Add(new MySqlParameter("vl_salarioliquido", folha.salarioliquido));
            parms.Add(new MySqlParameter("vl_vr", folha.vr));
            parms.Add(new MySqlParameter("vl_ir", folha.ir));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }




        public void Alterar(FolhaDePagamentoDTO folha)
        {
            string script =
           @"UPDATE tb_folhapagamento
                 SET
                  id_funcionario = @id_funcionario,
                  vl_transporte = @vl_transporte,
                  vl_inss = @vl_inss,
                  vl_salariobruto = @vl_salariobruto,
                  vl_salarioliquido = @vl_salarioliquido,
                  vl_vr = @vl_vr,
                  vl_ir = @vl_ir,
                  WHERE id_folhadepagamento = @id_folhadepagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folhadepagamento", folha.id_folhadepagamento));
            parms.Add(new MySqlParameter("id_funcionario", folha.id_funcionario));
            parms.Add(new MySqlParameter("vl_transporte", folha.transporte));
            parms.Add(new MySqlParameter("vl_inss", folha.inss));
            parms.Add(new MySqlParameter("vl_salariobruto", folha.salariobruto));
            parms.Add(new MySqlParameter("vl_salarioliquido", folha.salarioliquido));
            parms.Add(new MySqlParameter("vl_vr", folha.vr));
            parms.Add(new MySqlParameter("vl_ir", folha.ir));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }





        public void Remover(int id_folhadepagamento)
        {
            string script =
            @"DELETE FROM tb_folhadepagamento WHERE id_folhadepagamento = @id_folhadepagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folhadepagamento", id_folhadepagamento));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }





        public List<FolhaDePagamentoDTO> Consultar()
        {
            string script =
              @"SELECT * FROM tb_folhapagamento";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FolhaDePagamentoDTO> folhas = new List<FolhaDePagamentoDTO>();
            while (reader.Read())
            {

                FolhaDePagamentoDTO novafolha = new FolhaDePagamentoDTO();
                novafolha.id_folhadepagamento = reader.GetInt32("id_folhadepagamento");
                novafolha.id_funcionario = reader.GetInt32("id_funcionario");
                novafolha.transporte = reader.GetDecimal("vl_transporte");
                novafolha.inss = reader.GetDecimal("vl_inss");
                novafolha.salariobruto = reader.GetDecimal("vl_salariobruto");
                novafolha.salarioliquido = reader.GetDecimal("vl_salarioliquido");
                novafolha.vr = reader.GetDecimal("vl_vr");
                novafolha.ir = reader.GetDecimal("vl_ir");

                folhas.Add(novafolha);

            }
            return folhas;
        }
    }
}