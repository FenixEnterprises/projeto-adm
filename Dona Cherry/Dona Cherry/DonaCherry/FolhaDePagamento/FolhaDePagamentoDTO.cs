﻿namespace Dona_Cherry.DonaCherry.FolhaDePagamento
{
    class FolhaDePagamentoDTO
    {
        public int id_folhadepagamento { get; set; }
        public int id_funcionario { get; set; }
        public decimal transporte { get; set; }
        public decimal inss { get; set; }
        public decimal salariobruto { get; set; }
        public decimal salarioliquido { get; set; }
        public decimal vr { get; set; }
        public decimal ir { get; set; }

    }
}