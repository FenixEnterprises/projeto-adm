﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.PedidoFornecedor
{
    class PedidoFornecedorDatabase
    {
        public int Salvar(PedidoFornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_pedidofornecedor (nm_fornecedor, nm_funcionario, ds_quantidadedeproduto, dt_datapedido, dt_dataentrega, ds_valorpedido, ds_formadepagamento,) 
                                   VALUES (@nm_fornecedor, @nm_funcionario, @ds_quantidadedeproduto, @ds_datapedido, @ds_dataentrega, @ds_valorpedido, @ds_formadepagamento,)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", dto.fornecedor));
            parms.Add(new MySqlParameter("nm_funcionario", dto.funcionario));
            parms.Add(new MySqlParameter("ds_quantidadedeprodutos", dto.quantidadedeprodutos));
            parms.Add(new MySqlParameter("dt_datapedido ", dto.datapedido));
            parms.Add(new MySqlParameter("dt_dataentrega", dto.dataentrega));
            parms.Add(new MySqlParameter("ds_valorpedido", dto.valorpedido));
            parms.Add(new MySqlParameter("ds_formadepagamento", dto.formapagamento));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        

     }

}
