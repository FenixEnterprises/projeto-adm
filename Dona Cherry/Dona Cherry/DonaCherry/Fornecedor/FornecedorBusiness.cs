﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Fornecedor
{
    class FornecedorBusiness
    {

        FornecedorDatabase db = new FornecedorDatabase();


        public int Salvar(FornecedorDTO dto)
        {
            if (dto.nm_fantasia == string.Empty)
            {
                throw new ArgumentException("O nome fantasia é obrigatório");
            }
            if (dto.nm_razaosocial == string.Empty)
            {
                throw new ArgumentException("A razão social é obrigatório");
            }
            if (dto.ds_CNPJ == string.Empty)
            {
                throw new ArgumentException("O CPF ou CNPJ é obrigatorio");
            }

            if (dto.ds_endereco == string.Empty)
            {
                throw new ArgumentException("O endereço é obrigatorio.");
            }

            if (dto.ds_CEP == string.Empty)
            {
                throw new ArgumentException("O CEP é obrigatorio.");
            }

            if (dto.ds_bairro == string.Empty)
            {
                throw new ArgumentException("O nome do bairro é obrigatorio.");
            }

            if (dto.ds_cidade == string.Empty)
            {
                throw new ArgumentException("O nome da cidade é obrigatorio.");
            }

            if (dto.ds_estado == string.Empty)
            {
                throw new ArgumentException("O nome do estado é obrigatorio.");
            }

            return db.Salvar(dto);
        }

        public void Alterar(FornecedorDTO dto)
        {

                db.Alterar(dto);
        }





        public void Remover(int id)
        {
            db.Remover(id);
        }




        public List<FornecedorDTO> Listar()
        {
            
            return db.Listar();
        }




        public FornecedorDTO ConsultarPorCNPJ(string CPFCNPJ)
        {
            return db.ConsultarPorCNPJ(CPFCNPJ);
        }

        public List<FornecedorDTO> Consultar(string fornecedor)
        {
            return db.Consultar(fornecedor);
        }
    }
}