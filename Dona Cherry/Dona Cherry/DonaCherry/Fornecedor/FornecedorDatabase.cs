﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Fornecedor
{
    class FornecedorDatabase
    {       
        
            public int Salvar(FornecedorDTO dto)
            {
                string script =
                    @"insert into tb_fornecedor (nm_fantasia, nm_razaosocial, ds_CNPJ, ds_endereco, ds_CEP, ds_bairro, ds_cidade, ds_estado, ds_complemento)
                                         VALUES (@nm_fantasia, @nm_razaosocial, @ds_CNPJ, @ds_endereco, @ds_CEP, @ds_bairro, @ds_cidade, @ds_estado, @ds_complemento)";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("nm_fantasia", dto.nm_fantasia));
                parms.Add(new MySqlParameter("nm_razaosocial", dto.nm_razaosocial));
                parms.Add(new MySqlParameter("ds_CNPJ", dto.ds_CNPJ));
                parms.Add(new MySqlParameter("ds_endereco", dto.ds_endereco));
                parms.Add(new MySqlParameter("ds_CEP", dto.ds_CEP));
                parms.Add(new MySqlParameter("ds_bairro", dto.ds_bairro));
                parms.Add(new MySqlParameter("ds_cidade", dto.ds_cidade));
                parms.Add(new MySqlParameter("ds_estado", dto.ds_estado));
                parms.Add(new MySqlParameter("ds_complemento", dto.ds_complemento));


            Database db = new Database();
                return db.ExecuteInsertScriptWithPk(script, parms);

            }

        public FornecedorDTO ConsultarPorCNPJ(string CNPJ)
        {
            string script = @"SELECT * FROM tb_fornecedor WHERE ds_CNPJ = @ds_CNPJ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_CNPJ", CNPJ));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FornecedorDTO dto = null;
            if (reader.Read())
            {
                dto = new FornecedorDTO();

                dto.id_fornecedor = reader.GetInt32("id_fornecedor");
                dto.nm_razaosocial = reader.GetString("nm_razaosocial");
                dto.ds_CNPJ = reader.GetString("ds_CNPJ");
            }
            reader.Close();

            return dto;
        }


        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.id_fornecedor = reader.GetInt32("id_fornecedor");
                dto.nm_fantasia = reader.GetString("nm_fantasia");
                dto.nm_razaosocial = reader.GetString("nm_razaosocial");
                dto.ds_CNPJ = reader.GetString("ds_CNPJ");
                dto.ds_endereco = reader.GetString("ds_endereco");
                dto.ds_CEP = reader.GetString("ds_CEP");
                dto.ds_cidade = reader.GetString("ds_cidade");
                dto.ds_bairro = reader.GetString("ds_bairro");
                dto.ds_estado = reader.GetString("ds_estado");
                dto.ds_complemento = reader.GetString("ds_complemento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor 
                                 SET nm_fantasia     =@nm_fantasia,
                                     nm_razaosocial  =@nm_razaosocial,
                                     ds_CNPJ         =@ds_CNPJ,
                                     ds_endereco     =@ds_endereco,
                                     ds_CEP          =@ds_CEP,
                                     ds_bairro       =@ds_bairro,
                                     ds_cidade       =@ds_cidade,
                                     ds_estado       =@ds_estado,
                                     ds_complemento  =@ds_complemento,
                               WHERE id_funcionario  =@id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fantasia", dto.nm_fantasia));
            parms.Add(new MySqlParameter("nm_razaosocial", dto.nm_razaosocial));
            parms.Add(new MySqlParameter("ds_CNPJ", dto.ds_CNPJ));
            parms.Add(new MySqlParameter("ds_endereco", dto.ds_endereco));
            parms.Add(new MySqlParameter("ds_CEP", dto.ds_CEP));
            parms.Add(new MySqlParameter("ds_bairro", dto.ds_bairro));
            parms.Add(new MySqlParameter("ds_cidade", dto.ds_cidade));
            parms.Add(new MySqlParameter("ds_estado", dto.ds_estado));
            parms.Add(new MySqlParameter("ds_complemento", dto.ds_complemento));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }


        public List<FornecedorDTO> Consultar(string fornecedor)
        {

            string script =
                @"SELECT * FROM tb_fornecedor WHERE nm_fantasia like @nm_fantasia";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fantasia", "%" + fornecedor + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FornecedorDTO> fornecedores = new List<FornecedorDTO>();
            while (reader.Read())
            {

                FornecedorDTO fornecedori = new FornecedorDTO();
                fornecedori.id_fornecedor = reader.GetInt32("id_fornecedor");
                fornecedori.nm_fantasia = reader.GetString("nm_fantasia");
                fornecedori.ds_CNPJ = reader.GetString("ds_CNPJ");
                fornecedori.nm_razaosocial = reader.GetString("nm_razaosocial");
                fornecedori.ds_endereco = reader.GetString("ds_endereco");
                fornecedori.ds_CEP = reader.GetString("ds_CEP");
                fornecedori.ds_complemento = reader.GetString("ds_complemento");
                fornecedori.ds_cidade = reader.GetString("ds_cidade");
                fornecedori.ds_bairro = reader.GetString("ds_bairro");
                fornecedori.ds_estado = reader.GetString("ds_estado");

                fornecedores.Add(fornecedori);

            }
            reader.Close();
            return fornecedores;

        }
    }
}

