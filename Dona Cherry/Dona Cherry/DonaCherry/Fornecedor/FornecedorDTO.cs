﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Fornecedor
{
    class FornecedorDTO
    {
        public int id_fornecedor { get; set; }
        public string nm_fantasia { get; set; }
        public string nm_razaosocial { get; set; }
        public string ds_CNPJ { get; set; }
        public string ds_endereco { get; set; }
        public string ds_CEP { get; set; }
        public string ds_bairro { get; set; }
        public string ds_cidade { get; set; }
        public string ds_estado { get; set; }
        public string ds_complemento { get; set; }
    }
}
