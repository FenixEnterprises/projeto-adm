﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Funcionário
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script =
                @"insert into tb_funcionario (nm_funcionario, ds_endereco, ds_telefonefixo, ds_CPF, ds_cep,ds_celular,dt_dataadmissao,ds_salario,ds_cargahoraria,
                                           ds_cargo,ds_RG,ds_cidade,ds_bairro,ds_estado,ds_email,ds_complemento,nm_usuario,ds_senha,bt_permissao_rh,bt_permissao_funcionario,bt_permissao_financeiro)
                             VALUES (@nm_funcionario, @ds_endereco, @ds_telefonefixo, @ds_CPF,@ds_cep,@ds_celular,@dt_dataadmissao,@ds_salario,@ds_cargahoraria,
                                         @ds_cargo,@ds_RG,@ds_cidade,@ds_bairro,@ds_estado,@ds_email,@ds_complemento,@nm_usuario,@ds_senha, @bt_permissao_rh, @bt_permissao_funcionario, bt_permissao_financeiro)";



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.nome));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_telefonefixo", dto.telefonefixo));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_celular", dto.celular));
            parms.Add(new MySqlParameter("dt_dataadmissao", dto.dataadmissao));
            parms.Add(new MySqlParameter("ds_salario", dto.salario));
            parms.Add(new MySqlParameter("ds_cargahoraria", dto.cargahoraria));
            parms.Add(new MySqlParameter("ds_cargo", dto.cargo));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_cidade", dto.cidade));
            parms.Add(new MySqlParameter("ds_bairro", dto.bairro));
            parms.Add(new MySqlParameter("ds_estado", dto.estado));
            parms.Add(new MySqlParameter("ds_email", dto.email));
            parms.Add(new MySqlParameter("ds_complemento", dto.complemento));
            parms.Add(new MySqlParameter("nm_usuario", dto.usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.senha));
            parms.Add(new MySqlParameter("bt_permissao_financeiro", dto.permissaofinanceiro));
            parms.Add(new MySqlParameter("bt_permissao_rh", dto.permissaorh));
            parms.Add(new MySqlParameter("bt_permissao_funcionario", dto.permissaofuncionario));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FuncionarioDTO> ListarPerfilFuncionario()
        {
            string script = "Select*from tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {

                FuncionarioDTO dto = new FuncionarioDTO();
                dto.id_funcionario = reader.GetInt32("id_funcionario");
                dto.nome = reader.GetString("nm_funcionario");
                dto.endereco = reader.GetString("ds_endereco");
                dto.telefonefixo = reader.GetString("ds_telefonefixo");
                dto.CPF = reader.GetString("ds_CPF");
                dto.cep = reader.GetString("ds_CEP");
                dto.cargo = reader.GetString("ds_cargo");
                dto.email = reader.GetString("ds_email");
                dto.estado = reader.GetString("ds_estado");


                lista.Add(dto);

            }

            reader.Close();
            return lista;
        }

        
        public FuncionarioDTO Logar(string usuario, string senha)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE nm_usuario = @nm_usuario AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", usuario));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.id_funcionario = reader.GetInt32("id_funcionario");
                funcionario.usuario = reader.GetString("nm_usuario");
                funcionario.senha = reader.GetString("ds_senha");
                funcionario.permissaorh = reader.GetBoolean("bt_permissao_rh");
                funcionario.permissaofinanceiro = reader.GetBoolean("bt_permissao_financeiro");
                funcionario.permissaofuncionario = reader.GetBoolean("bt_permissao_funcionario");
            }
            reader.Close();
            return funcionario;
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE tb_funcionario 
                                 SET nm_funcionario  =@nm_funcionario,
                                     ds_endereco     =@ds_endereco,
                                     ds_telefonefixo =@ds_telefonefixo
                                     ds_CPF          =@ds_CPF
                                     ds_CEP          =@ds_CEP
                                     ds_celular      =@ds_celular
                                     dt_dataadmissao =@dt_dataadmissao
                                     ds_salario      =@salario
                                     ds_cargahoraria =@cargahoraria
                                     ds_cargo        =@ds_cargo
                                     ds_RG           =@ds_rg
                                     ds_cidade       =@ds_cidade
                                     ds_bairro       =@ds_bairro
                                     ds_estado       =@ds_estado
                                     ds_email        =@ds_email
                                     ds_complemento  =@ds_complemento
                               WHERE id_funcionario  =@id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.id_funcionario));
            parms.Add(new MySqlParameter("nm_funcionario", dto.nome));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_telefonefixo", dto.telefonefixo));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_celular", dto.celular));
            parms.Add(new MySqlParameter("dt_dataadmissao", dto.dataadmissao));
            parms.Add(new MySqlParameter("ds_salario", dto.salario));
            parms.Add(new MySqlParameter("ds_cargahoraria", dto.cargahoraria));
            parms.Add(new MySqlParameter("ds_cargo", dto.cargo));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_cidade", dto.cidade));
            parms.Add(new MySqlParameter("ds_bairro", dto.bairro));
            parms.Add(new MySqlParameter("ds_estado", dto.estado));
            parms.Add(new MySqlParameter("ds_email", dto.email));
            parms.Add(new MySqlParameter("ds_complemento", dto.complemento));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public List<FuncionarioDTO> Consultar(string funcionario)
        {
            string script = @"SELECT * tb_funcionario WHERE nm_funcionario like @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", funcionario + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.id_funcionario = reader.GetInt32("id_funcionario");
                dto.nome = reader.GetString("nm_funcionario");
                dto.endereco = reader.GetString("ds_endereco");
                dto.telefonefixo = reader.GetString("ds_telefonefixo");
                dto.CPF = reader.GetString("ds_CPF");
                dto.cep = reader.GetString("ds_CEP");
                dto.cargo = reader.GetString("ds_cargo");
                dto.email = reader.GetString("ds_email");
                dto.estado = reader.GetString("ds_estado");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
    }
}
