﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Funcionário
{
    class FuncionarioDTO
    {
        public int id_funcionario { get; set; }
        public string nome { get; set; }
        public string cep { get; set; }
        public string celular { get; set; }
        public string telefonefixo { get; set; }
        public DateTime dataadmissao { get; set; }
        public string salario { get; set; }
        public string cargahoraria { get; set; }
        public string cargo { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public string cidade { get; set; }
        public string bairro { get; set; }
        public string estado { get; set; }
        public string email { get; set; }
        public string complemento { get; set; }
        public string endereco { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }

        public bool permissaorh{ get; set; }

        public bool permissaofinanceiro{ get; set; }

        public bool permissaofuncionario{ get; set; }
    }
}
