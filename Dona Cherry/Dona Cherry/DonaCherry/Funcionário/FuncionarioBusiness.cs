﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Funcionário
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO dto)
        {
            if (dto.nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            if (dto.endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório");
            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("Cpf é obrigatório");
            }
            if (dto.telefonefixo == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório");
            }

            if (dto.bairro == string.Empty)
            {
                throw new ArgumentException("Bairro é obrigatório");
            }

            if (dto.cargo == string.Empty)
            {
                throw new ArgumentException("cargo é obrigatório");
            }
            if (dto.cep == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório");
            }
            if (dto.cidade == string.Empty)
            {
                throw new ArgumentException("cidade é obrigatório");
            }
            if (dto.dataadmissao == DateTime.Now)
            {
                throw new ArgumentException("Data de admissão é obrigatório");
            }
            if (dto.cargahoraria == string.Empty)
            {
                throw new ArgumentException("carga horária  é obrigatória");
            }
            if (dto.RG == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório");
            }
            if (dto.usuario == string.Empty)
            {
                throw new ArgumentException("usuário é obrigatorio VAI FAZER O LOGIN");
            }
            if(dto.senha == string.Empty)
            {
                throw new ArgumentException("senha é obrigatório VAI FAZER O LOGIN");
            }
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);


        }


        public void Alterar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(dto);


        }





        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);
        }




        public List<FuncionarioDTO> ListarPerfilFuncionario()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.ListarPerfilFuncionario();
        }




        public List<FuncionarioDTO> Consultar(string funcionario)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Consultar(funcionario);
        }




        public FuncionarioDTO Logar(string usuario, string senha)
        {
            if (usuario == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório.");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(usuario, senha);

        }


    }
}


