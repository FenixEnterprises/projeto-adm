﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Pedido
{
    class PedidoItemDatabase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_produtovenda, id_pedidocliente) VALUES (@id_produtovenda, @id_pedidocliente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedidocliente", dto.IdPedido));
            parms.Add(new MySqlParameter("id_produtovenda", dto.IdProduto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedido_item WHERE id_pedidocliente = @id_pedidocliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedidocliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<PedidoItemDTO> ConsultarPorPedido(int idPedido)
        {
            string script = @"SELECT * FROM tb_pedido_item WHERE id_pedidocliente = @id_pedidocliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedidocliente", idPedido));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoItemDTO> lista = new List<PedidoItemDTO>();
            while (reader.Read())
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.Id = reader.GetInt32("id_pedido_item");
                dto.IdPedido = reader.GetInt32("id_pedidocliente");
                dto.IdProduto = reader.GetInt32("id_produtovenda");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


    }
}
