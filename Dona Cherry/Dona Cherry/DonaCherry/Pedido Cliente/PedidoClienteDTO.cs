﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.PedidoCliente
{
    class PedidoClienteDTO
    {
        public int id { get; set; }
        public int funcionarioid { get; set; }
        public int clienteid { get; set; }
        public string formadepagamento { get; set; }
        public decimal preco { get; set; }
        public DateTime data { get; set; }
    }
}
