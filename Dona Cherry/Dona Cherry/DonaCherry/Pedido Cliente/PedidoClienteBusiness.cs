﻿using Dona_Cherry.DB.ProdutoVenda;
using Loja_de_roupas.DB.Pedido;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.PedidoCliente
{
    class PedidoClienteBusiness
    {
        public int Salvar(PedidoClienteDTO pedido, List<ProdutoVendaDTO> produtos)
        {
            

            if (pedido.formadepagamento == string.Empty)
            {
                throw new ArgumentException("Forma de Pagamento é obrigatório.");
            }

            if (pedido.clienteid == 0)
            {
                throw new ArgumentException("Cliente é obrigatório.");
            }

            if (pedido.funcionarioid == 0)
            {
                throw new ArgumentException("Funcionário é obrigatório.");
            }
            

            PedidoClienteDatabase pedidoDatabase = new PedidoClienteDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);
            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoVendaDTO item in produtos)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.id;

                itemBusiness.Salvar(itemDto);
            }




            return idPedido;
        }

            
        

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoClienteDatabase pedidoDatabase = new PedidoClienteDatabase();
            return pedidoDatabase.Consultar(cliente);
        }

        


    }
}
       
    

       



