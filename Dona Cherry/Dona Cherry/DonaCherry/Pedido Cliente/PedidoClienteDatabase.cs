﻿using Dona_Cherry.DB.Base;
using Loja_de_roupas.DB.Pedido;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.PedidoCliente
{
    class PedidoClienteDatabase
    {
        public int Salvar(PedidoClienteDTO dto)
        {
            string script = @"INSERT INTO tb_pedidocliente 
                                         (funcionario_id_funcionario, cliente_id_cliente, ds_forma_pag, vl_preco, dt_data)
                                 VALUES (@funcionario_id_funcionario, @cliente_id_cliente, @ds_forma_pag, @vl_preco, @dt_data)";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("funcionario_id_funcionario", dto.funcionarioid));
            parms.Add(new MySqlParameter("cliente_id_cliente", dto.clienteid));
            parms.Add(new MySqlParameter("ds_forma_pag", dto.formadepagamento));
            parms.Add(new MySqlParameter("vl_preco",dto.preco));
            parms.Add(new MySqlParameter("dt_data", dto.data));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedidocliente WHERE id_pedidocliente = @id_pedidocliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedidocliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoConsultarView> lista = new List<PedidoConsultarView>();
            while (reader.Read())
            {
                PedidoConsultarView dto = new PedidoConsultarView();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.QtdItens = reader.GetInt32("qtd_itens");
                dto.Data = reader.GetDateTime("dt_venda");
                dto.Vendedor = reader.GetString("nm_funcionario");
                dto.FormadePagamento = reader.GetString("ds_forma_pag");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        

        public decimal CalcularCompra(int quantidade, decimal preco)
        {
            
            decimal total = preco * quantidade;

            return total;
        }
    }
}
