﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.DB.ProdutoVenda
{
    class ProdutoVendaBusiness
    {
          public int Salvar(ProdutoVendaDTO dto)
            {
            if(dto.descricao == string.Empty)
            {
                throw new ArgumentException("A descrição do produto é obrigatório.");
            }

            if(dto.nome == string.Empty)
            {
                throw new ArgumentException("O nome do Produto é obrigatório.");
            }

           if (dto.valor <= 0)
            {
                throw new ArgumentException("O valor é obrigatório.");
            }

            ProdutoVendaDatabase db = new ProdutoVendaDatabase();

            List<ProdutoVendaDTO> produtos = db.Consultar(dto.nome);
            if (produtos.Count > 0)
                {
                throw new ArgumentException("Produto Venda com mesmo nome já cadastrado.");
                }

            return db.Salvar(dto);
            }

        public void Alterar(ProdutoVendaDTO dto)
        {
            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            db.Alterar(dto);
        }

         public void Remover(int id)
        {
            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            db.Remover(id);
        }
        public List<ProdutoVendaDTO> Consultar(string produto)
        {
            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            return db.Consultar(produto);
        }

         public List<ProdutoVendaDTO> Listar()
        {
            ProdutoVendaDatabase db = new ProdutoVendaDatabase();
            return db.Listar();
        }
    }
}
