﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.DB.ProdutoVenda
{
    class ProdutoVendaDatabase
    {
         public int Salvar(ProdutoVendaDTO dto)
        {
            string script = @"INSERT INTO tb_produtovenda (nm_produto, ds_descricao, img_produto, vl_valor)
                                                   VALUES  (@nm_produto, @ds_descricao, @img_produto, @vl_valor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.nome));
            parms.Add(new MySqlParameter("ds_descricao", dto.descricao));
            parms.Add(new MySqlParameter("img_produto", dto.Imagem));
            parms.Add(new MySqlParameter("vl_valor", dto.valor));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
            }

        public void Alterar(ProdutoVendaDTO dto)
        {
            string script = @"UPDATE tb_produtovenda 
                                 SET nm_produto,           = @nm_produto,
                                     ds_descricao,         = @ds_descricao,
                                     img_produto,          = @img_produto,
                                     vl_valor,             = @vl_valor,
                               WHERE id_produtovenda       = @id_produtovenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtovenda", dto.id));
            parms.Add(new MySqlParameter("nm_produto", dto.nome));
            parms.Add(new MySqlParameter("ds_descricao", dto.descricao));
            parms.Add(new MySqlParameter("img_produto", dto.Imagem));
            parms.Add(new MySqlParameter("vl_valor", dto.valor));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
            {
            string script = @"DELETE FROM tb_produtovenda WHERE id_produtovenda = @id_produtovenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtovenda", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

         public List<ProdutoVendaDTO> Consultar(string produto)
        {
                        string script = @"SELECT * FROM tb_produtovenda WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produtovenda", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoVendaDTO> lista = new List<ProdutoVendaDTO>();
            while (reader.Read())
            {
                ProdutoVendaDTO dto = new ProdutoVendaDTO();
                dto.id = reader.GetInt32("id_produtovenda");
                dto.nome = reader.GetString("nm_produto");
                dto.descricao = reader.GetString("ds_descricao");
                dto.valor = reader.GetDecimal("vl_valor");

                 lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ProdutoVendaDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produtovenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoVendaDTO> lista = new List<ProdutoVendaDTO>();
            while (reader.Read())
            {

                ProdutoVendaDTO dto = new ProdutoVendaDTO();
                dto.id = reader.GetInt32("id_produtovenda");
                dto.nome = reader.GetString("nm_produto");
                dto.descricao = reader.GetString("ds_descricao");
                dto.valor = reader.GetDecimal("vl_valor");

                 lista.Add(dto);
                }

             reader.Close();

            return lista;

        }
    }
}
