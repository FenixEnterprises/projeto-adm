﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.DB.ProdutoVenda
{
    class ProdutoVendaDTO
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public string Imagem { get; set; }
        public decimal valor { get; set; }


    }
}
