﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.DB.Estoque
{
    class EstoqueBusiness
    {
        public int Salvar(EstoqueDTO dto)
        {

            EstoqueDatabase db = new EstoqueDatabase();

            if (dto.nm_produto== string.Empty)
            {
                throw new ArgumentException("O nome do produto é obrigatório");
            }
            if (dto.id_produto <= 0)
            {
                throw new ArgumentException("O produto é obrigatório");
            }

            return db.Salvar(dto);
        }

            public void Alterar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Alterar(dto);
        }
        public void Remover(int id)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Remover(id);
        }
        public List<EstoqueDTO> Consultar(string produto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Consultar(produto);
        }
        public List<EstoqueDTO> Listar()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar();
        }
    }
}
