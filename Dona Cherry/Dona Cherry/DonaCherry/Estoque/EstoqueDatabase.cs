﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.DB.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script = @"INSERT INTO tb_estoque (id_produto, qtd_totalprodutos) 
                                   VALUES (@id_produto, @qtd_totalprodutos)";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("id_produto", dto.id_produto));
            parms.Add(new MySqlParameter("qtd_totalprodutos", dto.qtd_totalprodutos));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque 
                                 SET qtd_totalprodutos  = @qtd_totalprodutos,
                                     WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("id_produto", dto.id_produto));
            parms.Add(new MySqlParameter("qtd_totalprodutos", dto.qtd_totalprodutos));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<EstoqueDTO> Consultar(string produto)
        {
            string script = @"select id_estoque, nm_produto, qtd_totalprodutos
           from tb_estoque 
           join tb_produtovenda
           on tb_produtovenda.id_produtovenda = tb_estoque.id_produto
            where nm_produto like @nm_produto;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {

                EstoqueDTO dto = new EstoqueDTO();
                dto.id_estoque = reader.GetInt32("id_estoque");
                dto.qtd_totalprodutos = reader.GetInt32("qtd_totalprodutos");
                dto.nm_produto = reader.GetString("nm_produto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }

        public List<EstoqueDTO> Listar()
        {
            string script = @"select id_estoque, nm_produto, qtd_totalprodutos
           from tb_estoque 
           join tb_produtovenda
           on tb_produtovenda.id_produtovenda = tb_estoque.id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {

                EstoqueDTO dto = new EstoqueDTO();
                dto.id_estoque = reader.GetInt32("id_estoque");
                dto.qtd_totalprodutos = reader.GetInt32("qtd_totalprodutos");
                dto.nm_produto = reader.GetString("nm_produto");

                lista.Add(dto);

            }
            reader.Close();

            return lista;
        }

      
        
    }
}

