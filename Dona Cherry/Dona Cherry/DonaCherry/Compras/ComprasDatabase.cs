﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.DonaCherry.Compras
{
    class ComprasDatabase
    {
        class FornecedorDatabase
        {

            public int Salvar(ComprasDTO dto)
            {
                string script =
                    @"insert into tb_compras (produto_id_produto, fornecedor_id_fornecedor, vl_valor, qtd_total, dt_datacompra);
                                       VALUES(@produto_id_produto, @fornecedor_id_fornecedor, @vl_valor, @qtd_total, @dt_datacompra)";
;
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("produto_id_produto", dto.id_produto));
                parms.Add(new MySqlParameter("fornecedor_id_fornecedor", dto.id_fornecedor));
                parms.Add(new MySqlParameter("vl_valor", dto.valor));
                parms.Add(new MySqlParameter("qtd_total", dto.quantidade));
                parms.Add(new MySqlParameter("dt_datacompra", dto.datacompra));
                


                Database db = new Database();
                return db.ExecuteInsertScriptWithPk(script, parms);

            }



            public List<ComprasDTO> Listar()
            {
                string script = @"SELECT * FROM tb_compras";

                List<MySqlParameter> parms = new List<MySqlParameter>();

                Database db = new Database();
                MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

                List<ComprasDTO> lista = new List<ComprasDTO>();
                while (reader.Read())
                {
                    ComprasDTO dto = new ComprasDTO();
                    dto.id_compras = reader.GetInt32("id_compras");
                    dto.id_produto = reader.GetInt32("produto_id_produto");
                    dto.id_fornecedor = reader.GetInt32("fornecedor_id_fornecedor");
                    dto.valor = reader.GetDecimal("vl:_valor");
                    dto.quantidade = reader.GetInt32("qtd_total");
                    dto.datacompra = reader.GetDateTime("dt_datacompra");
                    

                    lista.Add(dto);
                }
                reader.Close();

                return lista;
            }

            public void Remover(int id)
            {
                string script = @"DELETE FROM tb_compras WHERE id_compras = @id_compras";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("id_compras", id));

                Database db = new Database();
                db.ExecuteInsertScript(script, parms);
            }

            public void Alterar(ComprasDTO dto)
            {
                string script = @"UPDATE tb_compras 
                                 SET produto_id_produto   =@produto_id_produto,
                       fornecedor_id_fornecedor      =@fornecedor_id_fornecedor,
                                     vl_valor        =@vl_valor,
                                     qtd_total       =@qtd_total,
                                    dt_datacompra    =@dt_datacompra
                                 WHERE id_compras    =@id_compras";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("produto_id_produto", dto.id_produto));
                parms.Add(new MySqlParameter("fornecedor_id_fornecedor", dto.id_fornecedor));
                parms.Add(new MySqlParameter("vl_valor", dto.valor));
                parms.Add(new MySqlParameter("qtd_total", dto.quantidade));
                parms.Add(new MySqlParameter("dt_datacompra", dto.datacompra));
                

                Database db = new Database();
                db.ExecuteInsertScript(script, parms);

            }


            public List<ComprasDTO> ConsultarPorID(int idcompra)
            {
                string script = @"SELECT * FROM tb_compras WHERE id_compras = @id_compras";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("id_compras", idcompra));

                Database db = new Database();
                MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

                List<ComprasDTO> lista = new List<ComprasDTO>();
                while (reader.Read())
                {
                    ComprasDTO dto = new ComprasDTO();
                    dto.id_compras = reader.GetInt32("id_compras");
                    dto.id_produto = reader.GetInt32("produto_id_produto");
                    dto.id_fornecedor = reader.GetInt32("fornecedor_id_fornecedor");
                    dto.valor = reader.GetDecimal("vl:_valor");
                    dto.quantidade = reader.GetInt32("qtd_total");
                    dto.datacompra = reader.GetDateTime("dt_datacompra");


                    lista.Add(dto);
                }
                reader.Close();

                return lista;
            }
        }
    }
}
