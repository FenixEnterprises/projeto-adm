﻿using Dona_Cherry.Telas.PedidoFornecedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.DonaCherry.Compras
{
    class ComprasBusiness
    {
        


        public int Salvar(ComprasDTO dto)
        {
            if (dto.id_fornecedor == 0)
            {
                throw new ArgumentException("O fornecedor é obrigatório.");
            }
            if (dto.id_produto == 0)
            {
                throw new ArgumentException("O produto é obrigatório");
            }
            if (dto.valor == 0)
            {
                throw new ArgumentException("O valor é obrigatorio");
            }

            if (dto.quantidade == 0)
            {
                throw new ArgumentException("A quantidade é obrigatória.");
            }

            if (dto.datacompra == DateTime.MaxValue)
            {
                throw new ArgumentException("A data da compra é obrigatória.");
            }

            ComprasDatabase db = new ComprasDatabase();
            return Salvar(dto);
        }

        public void Alterar(ComprasDTO dto)
        {
            ComprasDatabase db = new ComprasDatabase();
            Alterar(dto);
        }





        public void Remover(int id)
        {
            ComprasDatabase db = new ComprasDatabase();
            Remover(id);
        }




        public List<ComprasDTO> Listar()
        {
            ComprasDatabase db = new ComprasDatabase();
            return Listar();
        }


        public List<ComprasDTO> ConsultarPorID(int idcompras)
        {
            ComprasDatabase db = new ComprasDatabase();
            return ConsultarPorID(idcompras);
        }
    }
}
