﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.DonaCherry.Compras
{
    class ComprasDTO
    {
        public int id_compras { get; set; }
        public int id_produto { get; set; }
        public int id_fornecedor { get; set; }
        public decimal valor { get; set; }
        public int quantidade { get; set; }
        public DateTime datacompra { get; set; }
    }
}
