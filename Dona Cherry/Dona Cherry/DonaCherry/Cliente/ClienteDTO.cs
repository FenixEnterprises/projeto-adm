﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Cliente
{
    public class ClienteDTO
    {
        public int id_cliente { get; set; }
        public string nome { get; set; }
        public string cidade { get; set; }
        public string bairro { get; set; }
        public string endereco { get; set; }
        public string cep { get; set; }
        public string cpf { get; set; }
        public string telefonefixo { get; set; }
        public string celular { get; set; }
        public string complemento { get; set; }


    }
}
