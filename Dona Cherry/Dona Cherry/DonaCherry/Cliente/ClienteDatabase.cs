﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_cliente, ds_cidade, ds_bairro, ds_endereco, ds_cep, ds_cpf, ds_telefonefixo, ds_celular, ds_complemento) 
                                             VALUES (@nm_cliente, @ds_cidade, @ds_bairro, @ds_endereco, @ds_cep, @ds_cpf, @ds_telefonefixo, @ds_celular, @ds_complemento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.nome));
            parms.Add(new MySqlParameter("ds_cidade", dto.cidade));
            parms.Add(new MySqlParameter("ds_bairro", dto.bairro));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_cpf", dto.cpf));
            parms.Add(new MySqlParameter("ds_telefonefixo", dto.telefonefixo));
            parms.Add(new MySqlParameter("ds_celular", dto.celular));
            parms.Add(new MySqlParameter("ds_complemento", dto.complemento));
           

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ClienteDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM tb_cliente WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente","%" + nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            if (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();

                dto.id_cliente = reader.GetInt32("id_cliente");
                dto.nome = reader.GetString("nm_cliente");
                dto.cpf = reader.GetString("ds_cpf");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            string script = @"SELECT * FROM tb_cliente WHERE ds_cpf = @ds_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cpf", cpf));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = null;
            if (reader.Read())
            {
                dto = new ClienteDTO();

                dto.id_cliente = reader.GetInt32("id_cliente");
                dto.nome = reader.GetString("nm_cliente");
                dto.cpf = reader.GetString("ds_cpf");
            }
            reader.Close();

            return dto;
        }


        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.id_cliente = reader.GetInt32("id_cliente");
                dto.nome = reader.GetString("nm_cliente");
                dto.cpf = reader.GetString("ds_cpf");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_cliente 
                                 SET nm_cliente    =@nm_cliente,
                                     ds_bairro     =@ds_bairro,
                                     ds_telefonefixo   =@ds_telefonefixo
                                     ds_CPF          =@ds_CPF
                                     ds_CEP          =@ds_CEP
                                     ds_celular      =@ds_celular
                                     dt_endereco     =@dt_endereco
                                     ds_cidade       =@ds_cidade
                                     WHERE id_cliente  =@id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.id_cliente));
            parms.Add(new MySqlParameter("nm_nome", dto.nome));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_telefonefixo", dto.telefonefixo));
            parms.Add(new MySqlParameter("ds_CPF", dto.cpf));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_celular", dto.celular));
            parms.Add(new MySqlParameter("ds_bairro", dto.bairro));
            parms.Add(new MySqlParameter("ds_cidade", dto.cidade));
      

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

    }
}