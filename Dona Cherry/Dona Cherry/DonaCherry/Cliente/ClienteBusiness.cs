﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            if (dto.nome == string.Empty)
            {
                throw new ArgumentException("O campo Nome é obrigatório");
            }
            if (dto.cpf == string.Empty)
            {
                throw new ArgumentException("O campo CPF é obrigatório");
            }
            if (dto.cep == string.Empty)
            {
                throw new ArgumentException("O campo CEP é obrigatório");
            }
            if (dto.bairro == string.Empty)
            {
                throw new ArgumentException("O campo bairro é obrigatório");
            }
            if (dto.endereco == string.Empty)
            {
                throw new ArgumentException("O campo endereço é obrigatório");
            }
            if (dto.cidade == string.Empty)
            {
                throw new ArgumentException("O campo cidade é obrigatório");
            }
            if (dto.telefonefixo == string.Empty)
            {
                throw new ArgumentException("O campo telefone fixo é obrigatório");
            }


            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);

        }

        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarPorCpf(cpf);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();

        }

        public void Remover(int id)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Remover(id);
        }
      

        public void Alterar(ClienteDTO dto)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Alterar(dto);
        }

        public List<ClienteDTO> Consultar(string nome)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Consultar(nome);
        }
    }
}

