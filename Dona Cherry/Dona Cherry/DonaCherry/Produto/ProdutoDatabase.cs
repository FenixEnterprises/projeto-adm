﻿using Dona_Cherry.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_produto,fornecedor_id_fornecedor, ds_preco, ds_marca, ds_peso, qtd_produto) 
                                              VALUES (@nm_produto,@fornecedor_id_fornecedor, @ds_preco, @ds_marca, @ds_peso, @qtd_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            
            parms.Add(new MySqlParameter("nm_produto", dto.produto));
            parms.Add(new MySqlParameter("fornecedor_id_fornecedor", dto.fornecedor_id_fornecedor));
            parms.Add(new MySqlParameter("ds_preco", dto.preco));
            parms.Add(new MySqlParameter("ds_marca", dto.marca));
            parms.Add(new MySqlParameter("ds_peso",dto.peso));
            parms.Add(new MySqlParameter("qtd_produto",dto.qtdproduto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET nm_produto  = @nm_produto,
                       fornecedor_id_fornecedor  = @fornecedor_id_fornecedor
                                     vl_preco    = @vl_preco,
                                     ds_marca    = @ds_marca,
                                     vl_peso     = @vl_peso,
                                     qtd_produto = @qtd_produto
                                WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            
            parms.Add(new MySqlParameter("nm_produto", dto.produto));
            parms.Add(new MySqlParameter("fornecedor_id_fornecedor", dto.fornecedor_id_fornecedor));
            parms.Add(new MySqlParameter("ds_marca", dto.marca));
            parms.Add(new MySqlParameter("ds_preco", dto.preco));
            parms.Add(new MySqlParameter("ds_peso",dto.peso));
            parms.Add(new MySqlParameter("qtd_produto",dto.qtdproduto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.id = reader.GetInt32("id_produto");
                dto.fornecedor_id_fornecedor = reader.GetInt32("fornecedor_id_fornecedor");
                dto.produto = reader.GetString("nm_produto");
                dto.preco = reader.GetDecimal("vl_preco");
                dto.peso = reader.GetDecimal("vl_peso");
                dto.qtdproduto = reader.GetInt32("qtd_produto");
                
                
                
                
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.id = reader.GetInt32("id_produto");
                dto.produto = reader.GetString("nm_produto");
                dto.fornecedor_id_fornecedor = reader.GetInt32("fornecedor_id_fornecedor");
                dto.preco = reader.GetDecimal("vl_preco");
                dto.peso = reader.GetDecimal("vl_peso");
                dto.qtdproduto = reader.GetInt32("qtd_produto");
                
                

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}

