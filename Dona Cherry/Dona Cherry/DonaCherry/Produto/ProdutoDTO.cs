﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Produto
{
    class ProdutoDTO
    {
        public int id { get; set; }
        public int fornecedor_id_fornecedor { get; set; }
        public decimal preco { get; set; }
        public string marca { get; set; }
        public string produto { get; set; }
        public decimal peso { get; set; }
        public int qtdproduto { get; set; }

        
    }
}
