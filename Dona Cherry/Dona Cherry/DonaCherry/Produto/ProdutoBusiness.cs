﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dona_Cherry.Telas.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            if(dto.produto == string.Empty)
            {
                throw new ArgumentException("Produto é obrigatório");
            }

            if (dto.fornecedor_id_fornecedor == 0)
            {
                throw new ArgumentException("Fornecedor é obrigatório.");
            }

            if (dto.preco == 0)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }

            if (dto.peso == 0)
            {
                throw new ArgumentException("peso é obrigatório.");
            }

            ProdutoDatabase db = new ProdutoDatabase();

            List<ProdutoDTO> produtos = db.Consultar(dto.produto);
            if (produtos.Count > 0)
            {
                throw new ArgumentException("Produto com mesmo nome já cadastrado.");
            }

            return db.Salvar(dto);
        }

        public void Alterar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Alterar(dto);
        }
        public void Remover(int id)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Remover(id);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }
    }
}
